package com.mybatis.mybatisx;

import com.mybatis.mybatisx.entity.PrimaryKeyEntity;
import lombok.Data;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/5 21:20
 * @Description
 */
@Data
public class User extends PrimaryKeyEntity {


    private String name;

}
