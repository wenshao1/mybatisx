package com.mybatis.mybatisx;

import com.mybatis.mybatisx.entity.Condition;
import com.mybatis.mybatisx.provider.BaseProviderBuilder;

import java.beans.IntrospectionException;
import java.util.List;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/5 21:20
 * @Description
 */
public class Test1 {

    public static void main(String[] args) throws IntrospectionException, IllegalAccessException {
//        Class<User> userClass = User.class;
//        Field[] declaredFields = userClass.getFields();
//
//        System.out.println(Arrays.toString(declaredFields));

//        Method[] declaredMethods = ReflectionUtils.getDeclaredMethods(User.class);
        /*Method[] declaredMethods = ReflectionUtils.getUniqueDeclaredMethods(User.class);

        for (Method declaredMethod : declaredMethods) {
            System.out.println(declaredMethod.getName());
        }*/

//        User user = new User();
//        user.setId(123L);
//        Field[] allFields = ReflectionUtils.getAllFields(User.class);
//        for (Field allField : allFields) {
//            allField.setAccessible(true);
//            System.out.println(allField.getName());
//            Object o = allField.get(user);
//            System.out.println(o);
//
//        }

        List<Condition> build = Condition
                .builder()
                .eq("id", 1)
                .or()
                .ne("name", "zhangsan")
                .and(
                        Condition
                                .builder()
                                .eq("stattus", 1)
                                .or()
                                .isNotNull("ieDel").build()
                )
                .in("type",new String[]{"1","2","3"})
                .build();

        BaseProviderBuilder baseProviderBuilder = new BaseProviderBuilder();
        String param = baseProviderBuilder.getWhere(null, build, "param");
        System.out.println(param);

    }




}
