package com.mybatis.mybatisx;

import com.mybatis.mybatisx.generate.TemplateGenerateMain;

/**
 * @ClassName Test3
 * @Description
 * @Author 温少
 * @Date 2020/12/21 5:11 下午
 * @Version V1.0
 **/
public class Test3 {
    public static void main(String[] args) {
        String url = "jdbc:mysql://47.104.99.175:3306/scucfinal";
        String username = "root";
        String password = "123456";

        String driverName = "com.mysql.cj.jdbc.Driver";
        String out="/Users/wenshao/work/mywork/power-admin";
        String path = "com.power.entity";
        TemplateGenerateMain templateGenerateMain = new TemplateGenerateMain(url, username, password, driverName, out, path);
        templateGenerateMain.generate();
    }
}
