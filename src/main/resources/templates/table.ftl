package ${packagePath};

import com.mybatis.mybatisx.annotation.*;
import com.mybatis.mybatisx.entity.PrimaryKeyEntity;
import lombok.*;
<#list pkgs as pkg>
import ${pkg};
</#list>

/**
* ${table.javaName}
* ${table.comment}
*/
@Data
@Table("${table.name}")
@NoArgsConstructor
public class ${table.javaName} extends PrimaryKeyEntity {

<#list table.columns as column>
<#if !column.isKey>
    /**
    * ${column.comment}
    */
    @Column("${column.columnName}")
    private  ${column.javaType.type} ${column.javaName};
</#if>
</#list>
}
