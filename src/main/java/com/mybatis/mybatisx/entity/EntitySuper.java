package com.mybatis.mybatisx.entity;


import com.mybatis.mybatisx.annotation.Column;
import lombok.Data;

import java.io.Serializable;

@Data
public class EntitySuper extends TombstoneEntity implements Serializable {

    @Column
    private String createUser;
    @Column
    private Long createTime;
    @Column
    private String lastModifyUser;
    @Column
    private Long lastModifyTime;

}
