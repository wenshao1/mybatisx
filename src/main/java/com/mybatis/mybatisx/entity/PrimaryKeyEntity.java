package com.mybatis.mybatisx.entity;


import com.mybatis.mybatisx.annotation.PrimaryKey;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class PrimaryKeyEntity implements Serializable {


    @PrimaryKey
    private Long id;

}
