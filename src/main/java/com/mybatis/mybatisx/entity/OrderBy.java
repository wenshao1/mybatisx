package com.mybatis.mybatisx.entity;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName OrderBy
 * @Description
 * @Author 温少
 * @Date 2020/12/9 9:53 上午
 * @Version V1.0
 **/
@NoArgsConstructor
public class OrderBy {

    @Getter
    @Setter
    private String table;

    @Getter
    @Setter
    private String property;

    @Getter
    @Setter
    private OrderBy.Direction direction;

    private List<OrderBy> orderBys;

    public OrderBy(String table, String property, Direction direction) {
        this.table = table;
        this.property = property;
        this.direction = direction;
    }

    public OrderBy(List<OrderBy> orderBys) {
        this.orderBys = orderBys;
    }

    public static OrderBy builder(){
        return new OrderBy(new ArrayList<OrderBy>());
    }

    public OrderBy asc(String property){
        asc(null,property);
        return this;
    }

    public OrderBy asc(String table,String property){
        this.orderBys.add(new OrderBy(table,property, Direction.asc));
        return this;
    }

    public OrderBy desc(String property){
        this.desc(null,property);
        return this;
    }

    public OrderBy desc(String table,String property){
        this.orderBys.add(new OrderBy(table,property, Direction.desc));
        return this;
    }

    public OrderBy add(String table,String property,OrderBy.Direction direction){
        this.orderBys.add(new OrderBy(table,property, direction));
        return this;
    }

    public List<OrderBy> build(){
        return this.orderBys;
    }

    @Getter
    public static enum Direction {
        asc("ASC"),
        desc("DESC");



        private String value;
        private Direction(String value) {
            this.value = value;
        }

    }
}
