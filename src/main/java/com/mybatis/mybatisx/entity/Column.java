package com.mybatis.mybatisx.entity;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Column
 * @Description
 * @Author 温少
 * @Date 2020/12/9 9:41 上午
 * @Version V1.0
 **/
@NoArgsConstructor
public class Column {

    @Getter
    @Setter
    private String table;

    @Getter
    @Setter
    private String column;

    @Getter
    @Setter
    private String alias;

    private List<Column> columns;

    public Column(String table ,String column, String alias) {
        this.table = table;
        this.column = column;
        this.alias = alias;
    }

    public Column(List<Column> columns) {
        this.columns = columns;
    }

    public static Column builder(){
        return new Column(new ArrayList<Column>());
    }

    public Column add(String column){
        return this.add(null,column,null);
    }

    public Column add(String table,String column){
        return this.add(table,column,null);
    }

    public Column add(String table,String column,String alias){
        this.columns.add(new Column(table,column, alias));
        return this;
    }

    public List<Column> build(){
        return this.columns;
    }

}
