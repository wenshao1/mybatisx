package com.mybatis.mybatisx.entity;

import com.mybatis.mybatisx.utils.StringUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/6 20:12
 * @Description
 */
@Data
@Slf4j
public class Condition {

    /**
     * 列名称
     */
    private String property;

    /**
     * 所属表
     */
    private Class table;

    /**
     * 条件
     */
    private Condition.Operator operator;


    /**
     * 关联表的列名称
     */
    private String property2;

    /**
     * 关联表
     */
    private Class table2;


    /**
     * 包含只
     */
    private Object[] values;

    /**
     * 当前条件组
     */
    private List<Condition> conditions = new ArrayList<>();


    public Condition() {
    }

    public Condition(Class clazz, Operator operator, List<Condition> conditions) {
        this.table = clazz;
        this.operator = operator;
        this.conditions = conditions;
    }

    public Condition(String property, Operator operator) {
        this.property = property;
        this.operator = operator;
    }

    public Condition(Class table, String property, Operator operator) {
        this.property = property;
        this.operator = operator;
        this.table = table;
    }

    public Condition(Class table, String property, Operator operator, Object[] values) {
        this.property = property;
        this.operator = operator;
        this.values = values;
        this.table = table;
    }

    public Condition(String property, Operator operator, Object[] values) {
        this.property = property;
        this.operator = operator;
        this.values = values;
    }

    public static Condition builder() {
        return new Condition();
    }

    public Condition innerJoin(Class table, List<Condition> conditions) {
        if (StringUtils.isEmpty(table)) {
            log.warn("join table is null");
            return this;
        }
        if (CollectionUtils.isEmpty(conditions)) {
            log.warn("join conditions is null");
            return this;
        }
        Condition condition = new Condition(table, Operator.inner_join, conditions);
        this.conditions.add(condition);
        return this;
    }

    public Condition leftJoin(Class table, List<Condition> conditions) {
        if (StringUtils.isEmpty(table)) {
            log.warn("join table is null");
            return this;
        }
        if (CollectionUtils.isEmpty(conditions)) {
            log.warn("join conditions is null");
            return this;
        }
        Condition condition = new Condition(table, Operator.left_join, conditions);
        this.conditions.add(condition);
        return this;
    }

    public Condition rightJoin(Class table, List<Condition> conditions) {
        if (StringUtils.isEmpty(table)) {
            log.warn("join table is null");
            return this;
        }
        if (CollectionUtils.isEmpty(conditions)) {
            log.warn("join conditions is null");
            return this;
        }
        Condition condition = new Condition(table, Operator.right_join, conditions);
        this.conditions.add(condition);
        return this;
    }

    public Condition on(Class table1, String property1, Class table2, String property2) {
        if (Objects.isNull(table1)) {
            log.warn("table1 is null");
            return this;
        }
        if (Objects.isNull(property1)) {
            log.warn("table1.property1 is null");
            return this;
        }
        if (Objects.isNull(table2)) {
            log.warn("table2 is null");
            return this;
        }
        if (Objects.isNull(property2)) {
            log.warn("table2.property2 is null");
            return this;
        }

        Condition condition = new Condition();
        condition.setOperator(Operator.on);
        condition.setTable(table1);
        condition.setProperty(property1);
        condition.setTable2(table2);
        condition.setProperty2(property2);
        this.and();
        this.conditions.add(condition);
        return this;
    }


    public Condition eq(String property, Object value) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        if (Objects.isNull(value)) {
            return this;
        }
        Condition condition = new Condition(property, Operator.eq, new Object[]{value});
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition eqIsNull(String property, Object value, Boolean isCheckNull) {
        if (isCheckNull) {
            if (Objects.isNull(value)) {
                return this;
            }
        }
        return this.eq(property, value);
    }

    public Condition eq(Class table, String property, Object value) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        if (Objects.isNull(value)) {
            return this;
        }
        Condition condition = new Condition(table, property, Operator.eq, new Object[]{value});
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition eq(Class table, String property, Object value, Boolean isCheckNull) {
        if (isCheckNull) {
            if (Objects.isNull(value)) {
                return this;
            }
        }
        return this.eq(table, property, value);
    }


    public Condition ne(String property, Object value) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        if (Objects.isNull(value)) {
            return this;
        }
        Condition condition = new Condition(property, Operator.ne, new Object[]{value});
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition ne(Class table, String property, Object value) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        if (Objects.isNull(value)) {
            return this;
        }
        Condition condition = new Condition(table, property, Operator.ne, new Object[]{value});
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition gt(String property, Object value) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        if (Objects.isNull(value)) {
            return this;
        }
        Condition condition = new Condition(property, Operator.gt, new Object[]{value});
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition gt(Class table, String property, Object value) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        if (Objects.isNull(value)) {
            return this;
        }
        Condition condition = new Condition(table, property, Operator.gt, new Object[]{value});
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition lt(String property, Object value) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        if (Objects.isNull(value)) {
            return this;
        }
        Condition condition = new Condition(property, Operator.lt, new Object[]{value});
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition lt(Class table, String property, Object value) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        if (Objects.isNull(value)) {
            return this;
        }
        Condition condition = new Condition(table, property, Operator.lt, new Object[]{value});
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition ge(String property, Object value) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        if (Objects.isNull(value)) {
            return this;
        }
        Condition condition = new Condition(property, Operator.ge, new Object[]{value});
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition ge(Class table, String property, Object value) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        if (Objects.isNull(value)) {
            return this;
        }
        Condition condition = new Condition(table, property, Operator.ge, new Object[]{value});
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition le(String property, Object value) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        Condition condition = new Condition(property, Operator.le, new Object[]{value});
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition le(Class table, String property, Object value) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        Condition condition = new Condition(table, property, Operator.le, new Object[]{value});
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition like(String property, Object value) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        if (Objects.isNull(value)) {
            return this;
        }
        Condition condition = new Condition(property, Operator.like, new Object[]{"%" + value + "%"});
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition like(Class table, String property, Object value) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        if (Objects.isNull(value)) {
            return this;
        }
        Condition condition = new Condition(table, property, Operator.like, new Object[]{"%" + value + "%"});
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition in(String property, Collection<?> collection) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        if (CollectionUtils.isEmpty(collection)) {
            log.warn("Collection is null");
            return this;
        }
        Condition condition = new Condition(property, Operator.in, collection.toArray(new Object[collection.size()]));
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition in(Class table, String property, Collection<?> collection) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        if (CollectionUtils.isEmpty(collection)) {
            log.warn("Collection is null");
            return this;
        }
        Condition condition = new Condition(table, property, Operator.in, collection.toArray(new Object[collection.size()]));
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition in(String property, Object... objects) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        if (Objects.isNull(objects)) {
            log.warn("Array is null");
            return this;
        }
        Condition condition = new Condition(property, Operator.in, objects);
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition in(Class table, String property, Object... objects) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        if (Objects.isNull(objects)) {
            log.warn("Array is null");
            return this;
        }
        Condition condition = new Condition(table, property, Operator.in, objects);
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition nin(String property, Collection<?> collection) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        if (CollectionUtils.isEmpty(collection)) {
            log.warn("Collection is null");
            return this;
        }
        Condition condition = new Condition(property, Operator.nin, collection.toArray(new Object[collection.size()]));
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition nin(Class table, String property, Collection<?> collection) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        if (CollectionUtils.isEmpty(collection)) {
            log.warn("Collection is null");
            return this;
        }
        Condition condition = new Condition(table, property, Operator.nin, collection.toArray(new Object[collection.size()]));
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition nin(String property, Object... objects) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        if (Objects.isNull(objects) || objects.length == 0) {
            log.warn("Array is null");
            return this;
        }
        Condition condition = new Condition(property, Operator.nin, objects);
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition nin(Class table, String property, Object... objects) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        if (Objects.isNull(objects) || objects.length == 0) {
            log.warn("Array is null");
            return this;
        }
        Condition condition = new Condition(table, property, Operator.nin, objects);
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition isNull(String property) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        Condition condition = new Condition(property, Operator.isNull);
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition isNull(Class table, String property) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        Condition condition = new Condition(table, property, Operator.isNull);
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition isNotNull(String property) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        Condition condition = new Condition(property, Operator.isNotNull);
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition isNotNull(Class table, String property) {
        if (StringUtils.isEmpty(property)) {
            log.warn("property is null");
            return this;
        }
        Condition condition = new Condition(table, property, Operator.isNotNull);
        this.and();
        this.conditions.add(condition);
        return this;
    }

    public Condition and() {
        if (CollectionUtils.isEmpty(this.conditions)) {
            return this;
        }
        Condition preCondition = this.conditions.get(this.conditions.size() - 1);
        if (!this.isAndOrNotConditions(preCondition)) {
            Condition condition = new Condition();
            condition.setOperator(Operator.and);
            this.conditions.add(condition);
        }
        return this;
    }

    public Condition and(List<Condition> conditions) {
        Condition preCondition = this.conditions.get(this.conditions.size() - 1);
        if (!this.isAndOrNotConditions(preCondition)) {
            this.and();
        }
        Condition condition = new Condition();
        condition.setOperator(Operator.and);
        condition.setConditions(conditions);
        this.conditions.add(condition);
        return this;
    }

    public Condition and(Condition... objects) {
        Condition preCondition = this.conditions.get(this.conditions.size() - 1);
        if (!this.isAndOrNotConditions(preCondition)) {
            this.and();
        }
        Condition condition = new Condition();
        condition.setOperator(Operator.and);
        condition.setConditions(Arrays.asList(objects));
        this.conditions.add(condition);
        return this;
    }

    public Condition or() {
        if (CollectionUtils.isEmpty(this.conditions)) {
            return this;
        }
        Condition preCondition = this.conditions.get(this.conditions.size() - 1);
        if (!this.isAndOrNotConditions(preCondition)) {
            Condition condition = new Condition();
            condition.setOperator(Operator.or);
            this.conditions.add(condition);
        }

        return this;
    }

    public Condition or(List<Condition> conditions) {
        Condition preCondition = this.conditions.get(this.conditions.size() - 1);
        if (!this.isAndOrNotConditions(preCondition)) {
            this.or();
        }
        Condition condition = new Condition();
        condition.setOperator(Operator.or);
        condition.setConditions(conditions);
        this.conditions.add(condition);
        return this;
    }

    public Condition or(Condition... objects) {
        Condition preCondition = this.conditions.get(this.conditions.size() - 1);
        if (!this.isAndOrNotConditions(preCondition)) {
            this.or();
        }
        Condition condition = new Condition();
        condition.setOperator(Operator.or);
        condition.setConditions(Arrays.asList(objects));
        this.conditions.add(condition);
        return this;
    }

    public Boolean isAndOrNotConditions(Condition condition) {
        if ((Objects.equals(condition.getOperator(), Operator.and)
                || Objects.equals(condition.getOperator(), Operator.or)
                || Objects.equals(condition.getOperator(), Operator.left_join)
                || Objects.equals(condition.getOperator(), Operator.right_join)
                || Objects.equals(condition.getOperator(), Operator.inner_join)
        )
//                && CollectionUtils.isEmpty(condition.conditions)
        ) {
            return true;
        } else {
            return false;
        }
    }

    public List<Condition> build() {
        return this.conditions;
    }


    public static enum Operator {
        inner_join(" INNER JOIN "),
        left_join(" LEFT JOIN "),
        right_join(" RIGHT JOIN "),
        on("="),
        and("AND"),
        or("OR"),
        eq("="),
        ne("!="),
        gt(">"),
        lt("<"),
        ge(">="),
        le("<="),
        like("LIKE"),
        in("IN"),
        nin("NOT IN"),
        isNull("IS NULL"),
        isNotNull("IS NOT NULL");

        private String value;

        private Operator(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

        public static Condition.Operator fromString(String value) {
            return valueOf(value.toLowerCase());
        }
    }


}
