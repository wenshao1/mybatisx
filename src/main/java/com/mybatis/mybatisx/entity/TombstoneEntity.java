package com.mybatis.mybatisx.entity;

import com.mybatis.mybatisx.annotation.Column;
import lombok.Data;

import java.io.Serializable;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/5 15:56
 * @Description  逻辑删除
 */
@Data
public class TombstoneEntity extends PrimaryKeyEntity implements Serializable {
    @Column
    private Integer isDel;
}
