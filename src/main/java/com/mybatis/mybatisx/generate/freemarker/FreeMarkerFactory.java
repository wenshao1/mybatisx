package com.mybatis.mybatisx.generate.freemarker;

import com.mybatis.mybatisx.generate.connection.JdbcUtil;
import com.mybatis.mybatisx.generate.enums.IColumnType;
import com.mybatis.mybatisx.utils.StringUtils;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/13 21:47
 * @Description
 */
@Slf4j
public class FreeMarkerFactory {

    private Configuration configuration;

    public FreeMarkerFactory(){
        this.getClass().getResource("static");
        this.configuration = new Configuration();
        this.configuration.setClassForTemplateLoading(FreeMarkerFactory.class,"/templates/");
        this.configuration.setDefaultEncoding("utf-8");
    }

    private Template getTemplate(String name){
        try {
            return this.configuration.getTemplate(name);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void generate(String outpath,String packagePath, List<JdbcUtil.TableInfo> tableInfos,Boolean isCover){

        Template template = this.getTemplate("table.ftl");

        try {
            for (JdbcUtil.TableInfo tableInfo : tableInfos) {
                String filePath = outpath+File.separator+tableInfo.getJavaName()+".java";
                if(isCover){
                    File file = new File(filePath);
                    if(file.exists()){
                        file.delete();
                    }
                }

                Set<String> pkgs = tableInfo.getColumns().stream()
                        .map(JdbcUtil.Column::getJavaType)
                        .filter(x -> !StringUtils.isEmpty(x.getPkg()))
                        .map(IColumnType::getPkg)
                        .collect(Collectors.toSet());
                Map<String, Object> params = new HashMap<>();
                params.put("pkgs",pkgs);
                params.put("table",tableInfo);
                params.put("packagePath",packagePath);
                FileWriter fileWriter = new FileWriter(filePath);

                template.process(params,fileWriter);
                fileWriter.close();

                log.info("表：{}[{}],生成完成",tableInfo.getJavaName(),tableInfo.getComment());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        FreeMarkerFactory freeMarkerFactory = new FreeMarkerFactory();
        freeMarkerFactory.generate(null,null,null,null);
    }

}
