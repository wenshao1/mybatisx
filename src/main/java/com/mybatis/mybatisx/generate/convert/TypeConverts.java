package com.mybatis.mybatisx.generate.convert;

import com.mybatis.mybatisx.generate.enums.IColumnType;
import com.mybatis.mybatisx.generate.select.BranchBuilder;
import com.mybatis.mybatisx.generate.select.Selector;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/13 17:03
 * @Description
 */
public class TypeConverts {


    /**
     * 使用指定参数构建一个选择器
     *
     * @param param 参数
     * @return 返回选择器
     */
    static Selector<String, IColumnType> use(String param) {
        return new Selector<>(param.toLowerCase());
    }

    /**
     * 这个分支构建器用于构建用于支持 {@link String#contains(CharSequence)} 的分支
     *
     * @param value 分支的值
     * @return 返回分支构建器
     * @see #containsAny(CharSequence...)
     */
    static BranchBuilder<String, IColumnType> contains(CharSequence value) {
        return BranchBuilder.of(s -> s.contains(value));
    }

    /**
     * @see #contains(CharSequence)
     */
    static BranchBuilder<String, IColumnType> containsAny(CharSequence... values) {
        return BranchBuilder.of(s -> {
            for (CharSequence value : values) {
                if (s.contains(value)) return true;
            }
            return false;
        });
    }

}
