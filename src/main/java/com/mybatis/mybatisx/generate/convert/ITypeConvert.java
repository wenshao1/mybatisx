package com.mybatis.mybatisx.generate.convert;

import com.mybatis.mybatisx.generate.enums.IColumnType;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/13 17:09
 * @Description
 */
public interface ITypeConvert {


    /**
     * 执行类型转换
     *
     * @param fieldType    字段类型
     * @return ignore
     */
    IColumnType processTypeConvert(String fieldType);
}
