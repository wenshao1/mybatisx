package com.mybatis.mybatisx.generate.convert;


import com.mybatis.mybatisx.generate.enums.IColumnType;

import static com.mybatis.mybatisx.generate.enums.DbColumnType.*;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/13 17:10
 * @Description
 */
public class DB2TypeConvert implements ITypeConvert{
    @Override
    public IColumnType processTypeConvert(String fieldType) {
        return TypeConverts.use(fieldType)
                .test(TypeConverts.containsAny("char", "text", "json", "enum").then(STRING))
                .test(TypeConverts.contains("bigint").then(LONG))
                .test(TypeConverts.contains("smallint").then(BASE_SHORT))
                .test(TypeConverts.contains("int").then(INTEGER))
                .test(TypeConverts.containsAny("date", "time", "year").then(DATE))
                .test(TypeConverts.contains("bit").then(BOOLEAN))
                .test(TypeConverts.contains("decimal").then(BIG_DECIMAL))
                .test(TypeConverts.contains("clob").then(CLOB))
                .test(TypeConverts.contains("blob").then(BLOB))
                .test(TypeConverts.contains("binary").then(BYTE_ARRAY))
                .test(TypeConverts.contains("float").then(FLOAT))
                .test(TypeConverts.contains("double").then(DOUBLE))
                .or(STRING);
    }

    public static void main(String[] args) {
        DB2TypeConvert db2TypeConvert = new DB2TypeConvert();
        IColumnType anInt = db2TypeConvert.processTypeConvert("int");
        System.out.println();
    }
}
