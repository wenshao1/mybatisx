package com.mybatis.mybatisx.generate.enums;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/13 17:04
 * @Description
 */
public interface IColumnType {

    /**
     * 获取字段类型
     *
     * @return 字段类型
     */
    String getType();

    /**
     * 获取字段类型完整名
     *
     * @return 字段类型完整名
     */
    String getPkg();
}
