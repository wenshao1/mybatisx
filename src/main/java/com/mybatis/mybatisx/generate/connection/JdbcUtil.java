package com.mybatis.mybatisx.generate.connection;

import com.mybatis.mybatisx.generate.convert.DB2TypeConvert;
import com.mybatis.mybatisx.generate.enums.IColumnType;
import com.mysql.cj.jdbc.ConnectionImpl;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;
import com.mybatis.mybatisx.generate.enums.DbColumnType;
import org.springframework.util.CollectionUtils;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/12 21:06
 * @Description
 */
public class JdbcUtil {



    public static Connection getConnection(String url,String username,String password,String driverName ){
        try {
            Driver driver = (Driver)Class.forName(driverName).newInstance();
            Properties info = new Properties();
            info.put("user", username);
            info.put("password", password);
            return driver.connect(url, info);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static List<TableInfo> getTable(Connection conn,String... table) throws SQLException {
        List<String> tables = Arrays.asList(table);
        String dbName = ((ConnectionImpl) conn).getDatabase();
        List<TableInfo> tableNotesAll = getTableNotesAll(conn,dbName);
        List<Column> columns = getColumns(conn, dbName);
        Map<String, List<Column>> collect = columns.stream().collect(Collectors.groupingBy(x -> x.getTableName()));
        for (TableInfo tableInfo : tableNotesAll) {
            tableInfo.setColumns(collect.get(tableInfo.getName()));
        }
        return tableNotesAll.stream().filter(x->{
            if(!CollectionUtils.isEmpty(tables)){
                return tables.contains(x.getName());
            }
            return true;
        }).collect(Collectors.toList());
    }

    public static List<Column> getColumns(Connection conn,String db){

        DB2TypeConvert db2TypeConvert = new DB2TypeConvert();

        List<Column> columns = new ArrayList<>();
        try {
            String sql = "SELECT t.TABLE_NAME , t.COLUMN_KEY,t.COLUMN_NAME ,\n" +
                    "       t.column_type,t.COLUMN_COMMENT \n" +
                    "FROM information_schema.columns t WHERE table_schema IN  ('"+db+"')";

            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet=preparedStatement.executeQuery();

            while (resultSet.next()){
                String columnType = resultSet.getString("column_type");
                columnType = columnType.contains("(") ? columnType.substring(0,columnType.indexOf("(")) : columnType;
                IColumnType iColumnType = db2TypeConvert.processTypeConvert(columnType);
                Column column = new Column(resultSet.getString("TABLE_NAME"), resultSet.getString("COLUMN_NAME"),
                        resultSet.getString("column_type"), resultSet.getString("COLUMN_COMMENT"),
                        Objects.equals(resultSet.getString("COLUMN_KEY"),"PRI"),iColumnType);
                columns.add(column);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return columns;
    }


    private static List<TableInfo> getTableNotesAll(Connection con,String db){
        List<TableInfo> tableInfos = new ArrayList<>();
        try {
            String sql = "SELECT t.TABLE_SCHEMA ,t.TABLE_NAME,t.TABLE_ROWS ,t.TABLE_COLLATION ,t.TABLE_COMMENT \n" +
                    "FROM information_schema.tables t WHERE\n" +
                    "table_schema IN ('"+db+"')";

            PreparedStatement preparedStatement = con.prepareStatement(sql);
            ResultSet resultSet=preparedStatement.executeQuery();

            while (resultSet.next()){
                TableInfo tableInfo = new TableInfo(resultSet.getString("TABLE_NAME"), resultSet.getString("TABLE_COMMENT"),null);
                tableInfos.add(tableInfo);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return tableInfos;
    }

    @Data
    public static class TableInfo{

        private String name;

        private String javaName;

        private String comment;

        private List<Column> columns;

        public TableInfo(String name, String comment, List<Column> columns) {
            this.name = name;
            this.comment = comment;
            this.columns = columns;
        }
    }


    @Data
    public static class Column{

        private String tableName;

        private String columnName;

        private String javaName;

        private String type;

        private String comment;

        private Boolean isKey;

        public IColumnType javaType;

        public Column(String tableName, String columnName, String type, String comment, Boolean isKey, IColumnType javaType) {
            this.tableName = tableName;
            this.columnName = columnName;
            this.type = type;
            this.comment = comment;
            this.isKey = isKey;
            this.javaType = javaType;
        }
    }


    public static void main(String[] args) throws SQLException {
        String url = "jdbc:mysql://47.104.99.175:3306/test1";
        String username = "root";
        String password = "123456";
        String driverName = "com.mysql.cj.jdbc.Driver";
        List<TableInfo> table = getTable(getConnection(url, username, password, driverName),"person");
        System.out.println(table);

    }

}
