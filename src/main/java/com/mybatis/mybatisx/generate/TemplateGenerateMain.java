package com.mybatis.mybatisx.generate;

import com.mybatis.mybatisx.generate.connection.JdbcUtil;
import com.mybatis.mybatisx.generate.freemarker.FreeMarkerFactory;
import com.mybatis.mybatisx.utils.StringUtils;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/13 22:10
 * @Description
 */
@Data
public class TemplateGenerateMain {
    private String url;
    private String username;
    private String password;
    private String driverName;
    private String out;
    private String packagePath;
    private FreeMarkerFactory freeMarkerFactory = new FreeMarkerFactory();

    private String excludePre = "t_";
    private String suffix = "Entity";

    private Boolean isCover;

    public TemplateGenerateMain(String url, String username, String password, String driverName, String out,String packagePath) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.driverName = driverName;
        this.out = out+File.separator+"src"+File.separator+"main"+File.separator+"java"+File.separator+packagePath.replace(".", File.separator);
        File file = new File(this.out);
        if(!file.exists()){
            file.mkdirs();
        }
        this.packagePath = packagePath;
        this.isCover = false;
    }


    public void generate(String... table){
        Connection conn =null;
        try {
            conn = JdbcUtil.getConnection(url, username, password, driverName);
            List<JdbcUtil.TableInfo> tableInfos = JdbcUtil.getTable(conn,table);
            tableInfos.forEach(x->{
                x.setJavaName(StringUtils.firstCapitalize(StringUtils.underlineTOHump(x.getName().replaceFirst(excludePre,""))));;
                x.getColumns().forEach(y->{
                    y.setJavaName(StringUtils.underlineTOHump(y.getColumnName()));
                });
            });

            this.freeMarkerFactory.generate(this.out,this.packagePath,tableInfos,isCover);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        }

    }


    public static void main(String[] args) {

//        System.out.println(getClass().getResource("").getPath());
    }
}
