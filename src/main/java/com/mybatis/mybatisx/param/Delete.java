package com.mybatis.mybatisx.param;

import com.mybatis.mybatisx.config.MybatisxProperties;
import com.mybatis.mybatisx.entity.Condition;
import com.mybatis.mybatisx.entity.PrimaryKeyEntity;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/5 16:14
 * @Description
 */
@Data
@Builder
public class Delete implements Serializable {

    private MybatisxProperties mybatisxProperties;

    private List<Condition> conditions;

    private Class<? extends PrimaryKeyEntity> clazz;

    /**
     * 是否伪删除
     */
    private Boolean isPseudoDel;




}
