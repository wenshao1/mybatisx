package com.mybatis.mybatisx.param;

import com.mybatis.mybatisx.config.MybatisxProperties;
import com.mybatis.mybatisx.entity.Column;
import com.mybatis.mybatisx.entity.Condition;
import com.mybatis.mybatisx.entity.OrderBy;
import com.mybatis.mybatisx.rest.Pageable;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/5 16:13
 * @Description 查询对象
 */
@Data
@Builder
public class Select implements Serializable {

    private MybatisxProperties mybatisxProperties;

    private Class resultType;

    private List<Column> columns;

    private List<Condition> conditions;

    private List<Condition> joinTable;

    private List<OrderBy> orderBys;

    private Pageable pageable;

    private String sql;

    private Object[] objects;

}
