package com.mybatis.mybatisx.param;

import com.mybatis.mybatisx.config.MybatisxProperties;
import com.mybatis.mybatisx.entity.PrimaryKeyEntity;
import lombok.Builder;
import lombok.Data;
import org.springframework.cglib.beans.BeanMap;

import java.io.Serializable;
import java.util.Map;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/5 16:14
 * @Description
 */
@Data
@Builder
public class Insert implements Serializable {


    private MybatisxProperties mybatisxProperties;

    private  Object object;

}
