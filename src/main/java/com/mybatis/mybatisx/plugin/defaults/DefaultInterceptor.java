package com.mybatis.mybatisx.plugin.defaults;

import com.mybatis.mybatisx.config.MybatisxProperties;
import com.mybatis.mybatisx.entity.EntitySuper;
import com.mybatis.mybatisx.entity.TombstoneEntity;
import com.mybatis.mybatisx.enums.OperationEnum;
import com.mybatis.mybatisx.plugin.Interceptor;

/**
 * @ClassName DefaultInterceptor
 * @Description 数据填充
 * @Author 温少
 * @Date 2020/12/10 11:32 上午
 * @Version V1.0
 **/
public class DefaultInterceptor implements Interceptor {

    @Override
    public void intercept(MybatisxProperties properties, OperationEnum operation, Object object) {
        if(object == null || !(object instanceof TombstoneEntity)){
            return;
        }
        if(OperationEnum.INSERT.equals(operation)){
            TombstoneEntity entity = (TombstoneEntity) object;
            insert(entity);
        }

    }

    private void insert( TombstoneEntity entity){
        if(entity.getIsDel() == null){
            /** 未删除状态 */
            entity.setIsDel(0);
        }
    }
}
