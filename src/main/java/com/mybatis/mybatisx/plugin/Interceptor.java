package com.mybatis.mybatisx.plugin;

import com.mybatis.mybatisx.config.MybatisxProperties;
import com.mybatis.mybatisx.enums.OperationEnum;

/**
 * @ClassName Interceptor
 * @Description mybatisx 插件
 * @Author 温少
 * @Date 2020/12/10 11:18 上午
 * @Version V1.0
 **/
public interface Interceptor {

    void intercept(MybatisxProperties properties, OperationEnum operation,Object object);

}
