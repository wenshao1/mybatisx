package com.mybatis.mybatisx.config;

import com.mybatis.mybatisx.config.datasource.dynamic.DynamicDataSource;
import com.mybatis.mybatisx.dao.DataDao;
import com.mybatis.mybatisx.handler.BannerHandler;
import com.mybatis.mybatisx.mapper.BaseMapper;
import com.mybatis.mybatisx.plugin.Interceptor;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration;
import org.mybatis.spring.mapper.MapperFactoryBean;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.util.CollectionUtils;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/5 22:55
 * @Description
 */
@Configuration
@EnableConfigurationProperties({MybatisxProperties.class})
@ConditionalOnClass({SqlSessionFactory.class, SqlSessionFactoryBean.class})
@AutoConfigureBefore({MybatisAutoConfiguration.class})
@Slf4j
public class MybatisEnableAutoConfiguration {

    @Autowired
    MybatisxProperties mybatisxProperties;

    @Autowired(required = false)
    private List<org.apache.ibatis.plugin.Interceptor> interceptorList;


    @Bean
    public BannerHandler threadPoolBannerHandler() {
        return new BannerHandler(mybatisxProperties);
    }

    @Bean
    public MapperFactoryBean<BaseMapper> baseMapper(SqlSessionFactory sqlSessionFactory, SqlSessionTemplate sqlSessionTemplate) throws Exception {
        MapperFactoryBean<BaseMapper> mfb = new MapperFactoryBean();
        mfb.setMapperInterface(BaseMapper.class);
        mfb.setSqlSessionFactory(sqlSessionFactory);
        mfb.setSqlSessionTemplate(sqlSessionTemplate);
        return mfb;
    }

    @Bean
    public DataDao dataDao(MapperFactoryBean<BaseMapper> mapperFactoryBeans, List<Interceptor> interceptors) throws Exception {

        mybatisxProperties.setConfiguration(mapperFactoryBeans.getSqlSessionFactory().getConfiguration());
        DataDao dataDao = new DataDao();
        dataDao.setBaseMapper(mapperFactoryBeans.getObject());
        dataDao.setInterceptorChain(interceptors);
        dataDao.setMybatisxProperties(this.mybatisxProperties);
        log.info(">>>>> 初始化【DataDao】 完成");
        return dataDao;
    }
}