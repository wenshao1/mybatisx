package com.mybatis.mybatisx.config;

import com.mybatis.mybatisx.enums.DataBaseEnum;
import lombok.Data;
import org.apache.ibatis.session.Configuration;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/5 16:44
 * @Description
 */
@ConfigurationProperties(prefix = MybatisxProperties.PREFIX)
@Data
public class MybatisxProperties {

    public static final String PREFIX = "mybatisx";

    /**
     * 表前缀
     */
    private String tablepre;

    /**
     * 是否伪删除
     */
    private Boolean isPseudoDel = false;

    /**
     * 数据库类型
     */
    private DataBaseEnum dataBaseType = DataBaseEnum.MYSQL;

    /**
     * 是否显示sql
     */
    private Boolean isShowSql = Boolean.FALSE;

    /** 是否打印Banner */
    private Boolean banner = Boolean.TRUE;


    private Configuration configuration;

}
