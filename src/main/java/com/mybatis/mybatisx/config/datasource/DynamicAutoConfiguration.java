package com.mybatis.mybatisx.config.datasource;

import com.mybatis.mybatisx.config.datasource.druid.DruidAutoConfiguration;
import com.mybatis.mybatisx.config.datasource.dynamic.DynamicAspect;
import com.mybatis.mybatisx.config.datasource.dynamic.DynamicDataSource;
import com.mybatis.mybatisx.constant.DataSourceConstant;
import com.mybatis.mybatisx.exceptions.MybatisxException;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @ClassName DynamicAutoConfiguration
 * @Description
 * @Author 温少
 * @Date 2020/12/11 11:28 上午
 * @Version V1.0
 **/
@Configuration
@AutoConfigureBefore(DataSourceAutoConfiguration.class)
@AutoConfigureAfter(value = {MultiDataSource.class })
@Import(value = {DruidAutoConfiguration.class, DynamicAspect.class})
@ConditionalOnProperty(name = "spring.datasource.isMulti",havingValue = "true")
@ConditionalOnMissingBean(DataSource.class)
public class DynamicAutoConfiguration {

    @Resource
    private MultiDataSourceProperties multiDataSourceProperties;


    /**
     * @Author 温少
     * @Description  说明：配置多数据源
     * @Date 2020/12/11 2:30 下午
     * @Param  * @param dataSource
     * @param multiDataSource
     * @return com.mybatis.mybatisx.config.datasource.dynamic.DynamicDataSource
     **/
    @Bean(name = "dynamicDataSource")
    @ConditionalOnBean(value = {MultiDataSource.class})
    @ConditionalOnMissingBean(DataSource.class)
    public DynamicDataSource dynamicDataSource(MultiDataSource multiDataSource) throws Exception{

        if(multiDataSource.getDataSource().size() == 0){
            throw new MybatisxException("DataSource Size is null");
        }

        final DynamicDataSource dynamicDataSource = new DynamicDataSource();
        dynamicDataSource.setDefaultTargetDataSource(this.getDefaultDataSource(null,multiDataSource.getDataSource()));

        if(multiDataSource.getDataSource() !=null && multiDataSource.getDataSource().size() > 0){
            final Map<Object, Object> targetData = multiDataSource.getDataSource().entrySet()
                    .stream()
                    .filter(entry -> Objects.nonNull(entry.getValue()))
                    .collect(Collectors.toMap(HashMap.Entry::getKey, HashMap.Entry::getValue));

            dynamicDataSource.setTargetDataSources(targetData);
        }else {
            Map<Object, Object> targetData = new HashMap<>();
            dynamicDataSource.setTargetDataSources(targetData);
        }

        return dynamicDataSource;
    }

    /**
     * @Author 温少
     * @Description  说明：获取默认数据源
     * @Date 2020/12/11 2:30 下午
     * @Param  * @param dataSource
     * @param dataSources
     * @return javax.sql.DataSource
     **/
    public DataSource getDefaultDataSource(DataSource dataSource,Map<String, DataSource> dataSources){
        if(dataSource!=null){
            return dataSource;
        }
        if(dataSources == null && dataSources.size() == 0){
            throw new MybatisxException("No available data sources were found");
        }

        Map.Entry<String, MultiDataSourceProperties.DataSourceProperties> entry = multiDataSourceProperties.getMultiProperties()
                .entrySet()
                .stream()
                .filter(x -> x.getValue().isMaster())
                .findFirst()
                .orElseGet(()->null);
        if(entry == null){
            entry = multiDataSourceProperties.getMultiProperties().entrySet()
                    .stream()
                    .filter(x -> x.getKey().toLowerCase().contains(DataSourceConstant.MASTER))
                    .findFirst()
                    .orElseGet(()->null);
        }

        if(entry!=null){
            return dataSources.get(entry.getKey());
        }

        return dataSources.values()
                .stream().findFirst().get();
    }



}
