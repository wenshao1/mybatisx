package com.mybatis.mybatisx.config.datasource.druid;

import com.mybatis.mybatisx.config.datasource.DruidDataSourceFactory;
import com.mybatis.mybatisx.config.datasource.MultiDataSource;
import com.mybatis.mybatisx.config.datasource.MultiDataSourceProperties;
import com.mybatis.mybatisx.constant.DataSourceConstant;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.util.CollectionUtils;

import javax.sql.DataSource;
import javax.swing.*;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @ClassName DruidAutoConfiguration
 * @Description
 * @Author 温少
 * @Date 2020/12/11 10:19 上午
 * @Version V1.0
 **/
@Configuration
@EnableConfigurationProperties(MultiDataSourceProperties.class)
@AutoConfigureAfter(MultiDataSourceProperties.class)
@ConditionalOnClass(name = "com.alibaba.druid.pool.DruidDataSource")
//@ConditionalOnProperty("spring.datasource.dataSourcePropertiesMap")
public class DruidAutoConfiguration {


    /*@Configuration
    @AutoConfigureBefore(DataSourceAutoConfiguration.class)
    @Slf4j
    protected static class DruidMasterDataSourceConfiguration{

        @Autowired
        private MultiDataSourceProperties multiDataSourceProperties;

        @Bean
        @Primary
        public DataSource dataSource() throws SQLException {
            if(multiDataSourceProperties == null
                    || multiDataSourceProperties.getDataSourcePropertiesMap().size() == 0){
                return null;
            }

            List<MultiDataSourceProperties.DataSourceProperties> masters = multiDataSourceProperties.getDataSourcePropertiesMap().values()
                    .stream().filter(x -> x.isMaster()).collect(Collectors.toList());
            MultiDataSourceProperties.DataSourceProperties dataSourceProperties;
            if(!CollectionUtils.isEmpty(masters)){
                dataSourceProperties = masters.get(0);
            }else {
                Set<String> keySet = multiDataSourceProperties.getDataSourcePropertiesMap().keySet();
                List<String> collect = keySet.stream().filter(x -> x.toLowerCase().contains(DataSourceConstant.MASTER)).collect(Collectors.toList());
                if(!CollectionUtils.isEmpty(collect)){
                    dataSourceProperties = multiDataSourceProperties.getDataSourcePropertiesMap().get(collect.get(0));
                }else {
                    dataSourceProperties = multiDataSourceProperties.getDataSourcePropertiesMap().values().stream().findFirst().get();
                }
            }

            final DataSource dataSource = DruidDataSourceFactory.createDataSource(dataSourceProperties);
            log.info(">>>>> 初始化数据库连接（master - druid）：{}", dataSourceProperties.getUrl());
            return dataSource;
        }
    }*/

    @Slf4j
    @RequiredArgsConstructor
    @Configuration
    protected static class DruidMultiDataSourceConfiguration {

        @Autowired
        private MultiDataSourceProperties multiDataSourceProperties;

        @Bean
        @ConditionalOnMissingBean
        public MultiDataSource multiDataSource() throws Exception{
            Map<String, DataSource> dataSourcesMap = new HashMap<>();
            if (multiDataSourceProperties == null
                    || multiDataSourceProperties.getMultiProperties() == null
                    || multiDataSourceProperties.getMultiProperties().size() == 0) {
                throw new NullPointerException("Not Configuration MultiDataSourceProperties >> multiProperties");
            }

            Map<String, MultiDataSourceProperties.DataSourceProperties> dataSourcePropertiesMap = multiDataSourceProperties.getMultiProperties();

            dataSourcePropertiesMap.forEach((k,v)->{
                try {
                    dataSourcesMap.put(k,DruidDataSourceFactory.createDataSource(v));
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            });
            return new MultiDataSource(dataSourcesMap);
        }
    }
}
