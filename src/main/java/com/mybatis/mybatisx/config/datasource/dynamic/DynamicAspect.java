package com.mybatis.mybatisx.config.datasource.dynamic;

import com.mybatis.mybatisx.annotation.DBType;
import com.mybatis.mybatisx.constant.DataSourceConstant;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Objects;

/**
 * @Author 温少
 * @Description  说明：拦截数据库选择数据源注解
 * @Date 2020/12/11 5:44 下午
 * @Param  * @param null
 * @return
 **/
@Slf4j
@Aspect
public class DynamicAspect implements Ordered {

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE + 1;
    }

    /**
     * 声明数据库类型
     */
    @Pointcut("@annotation(com.mybatis.mybatisx.annotation.DBType)")
    protected void dynamicMethod() {
    }

    @Before("dynamicMethod()")
    public void before(JoinPoint joinPoint) {
        //数据库类型
        String dataType;
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        final Method method = signature.getMethod();
        DBType dataSourceType = method.getAnnotation(DBType.class);
        if (Objects.nonNull(dataSourceType)) {
            dataType = dataSourceType.name();
        } else {
            dataType = DataSourceConstant.MASTER;
        }
        DynamicHandler.set(dataType);
    }

    @AfterReturning("dynamicMethod()")
    public void after() {
        DynamicHandler.clean();
    }

    @AfterThrowing("dynamicMethod()")
    public void exception() {
        DynamicHandler.clean();
    }
}
