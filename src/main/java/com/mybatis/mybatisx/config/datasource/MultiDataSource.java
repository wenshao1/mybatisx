package com.mybatis.mybatisx.config.datasource;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.sql.DataSource;
import java.util.Map;

/**
 * @ClassName MultiDataSource
 * @Description
 * @Author 温少
 * @Date 2020/12/11 11:15 上午
 * @Version V1.0
 **/
@Data
@AllArgsConstructor
public class MultiDataSource {

    private Map<String, DataSource> dataSource;



}
