package com.mybatis.mybatisx.config.datasource.dynamic;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * @ClassName DynamicDataSource
 * @Description
 * @Author 温少
 * @Date 2020/12/11 11:30 上午
 * @Version V1.0
 **/
@Slf4j
@Data
@EqualsAndHashCode(callSuper = true)
public class DynamicDataSource extends AbstractRoutingDataSource {
    @Override
    protected Object determineCurrentLookupKey() {
        String type = DynamicHandler.get();
        log.info("current data source：{}",type);
        return type;
    }
}
