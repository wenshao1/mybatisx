package com.mybatis.mybatisx.config.datasource.dynamic;

import com.mybatis.mybatisx.constant.DataSourceConstant;

/**
 * @Author 温少
 * @Description  说明：设置数据源
 * @Date 2020/12/11 5:32 下午
 * @Param  * @param null
 * @return
 **/
public final class DynamicHandler {

    /**
     * 存储当前数据库 对应的 key
     */
    private static ThreadLocal<String> DATA_SOURCE_KEY = ThreadLocal.withInitial(() -> DataSourceConstant.MASTER);

    /**
     * 设置数据源
     */
    public static void set(String dataSourceKey) {
        DATA_SOURCE_KEY.set(dataSourceKey);
    }

    /**
     * 获取数据源
     */
    public static String get() {
        return DATA_SOURCE_KEY.get();
    }

    /**
     * 清空本地线程
     */
    public static void clean() {
        DATA_SOURCE_KEY.remove();
    }
}

