package com.mybatis.mybatisx.config.datasource;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.Ordered;

import java.util.Map;
import java.util.Properties;

/**
 * @ClassName MultiDataSourceProperties
 * @Description
 * @Author 温少
 * @Date 2020/12/11 10:04 上午
 * @Version V1.0
 **/
@ConfigurationProperties(prefix = MultiDataSourceProperties.PREFIX)
public class MultiDataSourceProperties implements Ordered {

    public static final String PREFIX = "spring.datasource";

    /**
     * 是否多数据源
     */
    @Setter
    @Getter
    private Boolean isMulti = false;

    @Setter
    @Getter
    private Map<String, DataSourceProperties> multiProperties ;

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE+1;
    }


    /**
     * @Author 温少
     * @Description  说明：数据库配置信息
     * @Date 2020/12/11 10:15 上午
     * @return
     **/
    @Data
    @NoArgsConstructor
    public static class DataSourceProperties{

        private String url;

        private String username;

        private String password;

        /** 是否主库 */
        private boolean isMaster = false;

        /**
         * 是否只读库
         */
        private boolean readonly;
        /**
         * 权重，方便负载均衡使用
         */
        private int weight;

        /**
         * 驱动
         */
        private String driverClassName;

        /** ---------------- **/
        /**
         * 连接池最大值
         */
        private Integer maxActive = 200;

        /**
         * 初始大小
         */
        private Integer initialSize = 10;

        /**
         * 连接等待最大时间
         */
        private Integer maxWait = 60000;

        /**
         * 连接池最小值
         */
        private Integer minIdle = 10;

        /** ---------------- **/
        /**
         * 有两个含义： 1)Destroy线程会检测连接的间隔时间2) testWhileIdle的判断依据，详细看testWhileIdle属性的说明
         */
        private Integer timeBetweenEvictionRunsMillis = 60000;

        /**
         * 配置一个连接在池中最小生存的时间，单位是毫秒
         */
        private Integer minEvictableIdleTimeMillis = 300000;

        private String validationQuery = "select 'x'";

        /**
         * ----------------
         **/
        private boolean testWhileIdle = true;
        private boolean testOnBorrow = true;
        private boolean testOnReturn = false;

        /**
         * ----------------
         **/
        private boolean poolPreparedStatements = true;

        private Integer maxPoolPreparedStatementPerConnectionSize = 20;

        private Integer psCacheSize;

        private String filters = "stat,slf4j";

        private Properties connectionProperties;
    }

}
