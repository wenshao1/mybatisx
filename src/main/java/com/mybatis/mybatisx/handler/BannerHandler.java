package com.mybatis.mybatisx.handler;

import com.mybatis.mybatisx.config.MybatisxProperties;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.ansi.AnsiColor;
import org.springframework.boot.ansi.AnsiOutput;
import org.springframework.boot.ansi.AnsiStyle;

/**
 * Dynamic thread pool print banner.
 *
 * @author chen.ma
 * @date 2021/6/20 16:34
 */
@Slf4j
@RequiredArgsConstructor
public class BannerHandler implements InitializingBean {

    @NonNull
    private final MybatisxProperties properties;


    private final String NAME = " :: MyBatis X :: ";

    private final int STRAP_LINE_SIZE = 50;

    @Override
    public void afterPropertiesSet() {
        printBanner();
    }

    private void printBanner() {
        String banner = " __  __    _  _    ___             _        _                     __  __  \n" +
                "|  \\/  |  | || |  | _ )   __ _    | |_     (_)     ___      o O O \\ \\/ /  \n" +
                "| |\\/| |   \\_, |  | _ \\  / _` |   |  _|    | |    (_-<     o       >  <   \n" +
                "|_|__|_|  _|__/   |___/  \\__,_|   _\\__|   _|_|_   /__/_   TS__[O] /_/\\_\\  \n" +
                "_|\"\"\"\"\"|_| \"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"| {======|_|\"\"\"\"\"| \n" +
                "\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'./o--000'\"`-0-0-' \n";

        if (properties.getBanner()) {
            String version = getVersion();
            version = (version != null) ? " (v" + version + ")" : "no version.";

            StringBuilder padding = new StringBuilder();
            while (padding.length() < STRAP_LINE_SIZE - (version.length() + NAME.length())) {
                padding.append(" ");
            }

            System.out.println(AnsiOutput.toString(banner, AnsiColor.GREEN, NAME, AnsiColor.DEFAULT,
                    padding.toString(), AnsiStyle.FAINT, version,  "\n"));
        }
    }

    public static String getVersion() {
        final Package pkg = BannerHandler.class.getPackage();
        return pkg != null ? pkg.getImplementationVersion() : "";
    }

}
