package com.mybatis.mybatisx.provider;

import com.mybatis.mybatisx.param.Delete;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.util.CollectionUtils;

import java.util.Map;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/5 16:27
 * @Description
 */
public class DeleteProviderBuilder extends BaseProviderBuilder{


    public String delete(Map<String, Delete> params){
        Delete delete = params.get("param");
        Boolean isPseudoDel = delete.getIsPseudoDel() == null ? delete.getMybatisxProperties().getIsPseudoDel():delete.getIsPseudoDel();
        SQL sql = new SQL();
        if(isPseudoDel){
            sql.UPDATE(super.getTableName(delete.getMybatisxProperties(),delete.getClazz()));
            sql.SET("is_del = 1");
        }else {
            sql.DELETE_FROM(super.getTableName(delete.getMybatisxProperties(),delete.getClazz()));
        }

        if(!CollectionUtils.isEmpty(delete.getConditions())){
            sql.WHERE(super.getWhere(delete.getMybatisxProperties(), delete.getConditions(), "param"));
        }

        return sql.toString();
    }


}
