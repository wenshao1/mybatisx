package com.mybatis.mybatisx.provider;

import com.mybatis.mybatisx.param.Insert;
import org.apache.ibatis.jdbc.SQL;

import java.util.List;
import java.util.Map;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/5 16:26
 * @Description 构建插入sql
 */
public class InsertProviderBuilder extends BaseProviderBuilder{

    public String insert(Map<String, Insert> params){
        Insert insert = params.get("param");
        SQL sql =new SQL();
        sql.INSERT_INTO(super.getTableName(insert.getMybatisxProperties(), insert.getObject().getClass()));
        super.setInsertColumnsAndValues(insert.getObject(), sql);
        return sql.toString();

    }


    public String insertBatch(Map<String, Insert> params){
        Insert insert = params.get("param");
        StringBuilder sql = new StringBuilder();
        List<Object> list = (List<Object>) insert.getObject();
        sql.append("insert into ").append(super.getTableName(insert.getMybatisxProperties(), list.get(0).getClass()));
        sql.append(super.setInsertBatchColumnsAndValues(list));
        return sql.toString();

    }

}
