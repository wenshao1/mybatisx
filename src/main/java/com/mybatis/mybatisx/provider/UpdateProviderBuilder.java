package com.mybatis.mybatisx.provider;

import com.mybatis.mybatisx.param.Update;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.util.CollectionUtils;

import java.util.Map;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/5 16:27
 * @Description
 */
public class UpdateProviderBuilder extends BaseProviderBuilder{

    public String update(Map<String, Update> params){
        Update update = params.get("param");
        SQL sql = new SQL();
        sql.UPDATE(super.getTableName(update.getMybatisxProperties(),update.getClazz()));
        sql.SET(super.updateSet(update.getIsUseNull(),update.getClazz(),update.getObject()));
        if(!CollectionUtils.isEmpty(update.getConditions())){
            sql.WHERE(super.getWhere(update.getMybatisxProperties(), update.getConditions(), "param"));
        }
        return sql.toString();
    }
}
