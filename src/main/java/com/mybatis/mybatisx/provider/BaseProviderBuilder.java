package com.mybatis.mybatisx.provider;

import com.mybatis.mybatisx.annotation.Column;
import com.mybatis.mybatisx.annotation.PrimaryKey;
import com.mybatis.mybatisx.annotation.Table;
import com.mybatis.mybatisx.config.MybatisxProperties;
import com.mybatis.mybatisx.constant.Constant;
import com.mybatis.mybatisx.entity.Condition;
import com.mybatis.mybatisx.exceptions.MybatisxException;
import com.mybatis.mybatisx.utils.AssertUtil;
import com.mybatis.mybatisx.utils.ReflectionUtils;
import com.mybatis.mybatisx.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/5 16:42
 * @Description
 */
@Slf4j
public class BaseProviderBuilder {


    /**
     * @Description 获取表名称
     * @param properties
     * @param clazz
     * @return
     */
    public String getTableName(MybatisxProperties properties , Class clazz){
        Table table = (Table) clazz.getAnnotation(Table.class);
        if(table == null){
            throw new MybatisxException("Entity %s is not @Table",clazz.getName());
        }

        if(!StringUtils.isEmpty(table.value())){
            return table.value();
        }
        return StringUtils.isEmpty(properties.getTablepre()) ? StringUtils.humpTOUnderline(clazz.getSimpleName()) : properties.getTablepre()+StringUtils.humpTOUnderline(clazz.getSimpleName());

    }


    /**
     * @Description 获取字段
     * @return
     */
    public String[] getTableColumns(Class clazz){
        try {
            List<String> tableFields = new ArrayList<>();
            Field[] fields = ReflectionUtils.getAllFields(clazz);
            for (Field field : fields) {
                if(Constant.ID.equals(field.getName())){
                    PrimaryKey primaryKey = field.getAnnotation(PrimaryKey.class);
                    if(!(primaryKey == null || "auto".equals(primaryKey.value()))){
                        tableFields.add(field.getName());
                    }
                    continue;
                }
                tableFields.add(getColumnName(field));
            }
            if(CollectionUtils.isEmpty(tableFields)){
                throw new MybatisxException("Entity %s field is null",clazz.getName());
            }

            return tableFields.toArray(new String[tableFields.size()]);

        }catch (Exception e){
            e.printStackTrace();
            throw new MybatisxException("Entity %s Error",clazz.getName());
        }
    }

    public void setInsertColumnsAndValues(Object object, SQL sql){
        try {
            Field[] fields = ReflectionUtils.getAllFields(object.getClass());
            if(fields.length == 0){
                throw new MybatisxException("Entity %s field is null",object.getClass().getName());
            }
            for (Field field : fields) {
                field.setAccessible(true);
                if(Constant.ID.equals(field.getName())){
                    PrimaryKey primaryKey = field.getAnnotation(PrimaryKey.class);
                    if(!(primaryKey == null || "auto".equals(primaryKey.value()))){
                        sql.VALUES(Constant.ID,this.getInsertAndUpdateValue(field));
                    }
                    continue;
                }
                sql.VALUES(getColumnName(field),this.getInsertAndUpdateValue(field));
            }

        }catch (Exception e){
            e.printStackTrace();
            throw new MybatisxException("Entity %s Error",object.getClass().getName());
        }
    }

    /**
     * @Author 温少
     * @Description 说明：组合批量插入的数据
     * @Date 2021/4/16 2:16 下午
     *  * @param object
     * @return void
     **/
    public String setInsertBatchColumnsAndValues(List<Object> list){
        AssertUtil.isEmpty(list);
        Object object = list.get(0);
        StringBuilder sql = new StringBuilder(Constant.SMALL_LEFT);
        try {
            Field[] fields = ReflectionUtils.getAllFields(object.getClass());
            if(fields.length == 0){
                throw new MybatisxException("Entity %s field is null",object.getClass().getName());
            }
            String columns = Stream.of(fields).map(field -> {
                field.setAccessible(true);
                return getColumnName(field);
            }).collect(Collectors.joining(Constant.COMMA));
            sql.append(columns).append(Constant.SMALL_RIGHT).append(" VALUES ");
            for (int i = 0; i < list.size(); i++) {
                int index = i;
                String values = Stream.of(fields).map(field -> {
                    field.setAccessible(true);
                    return getInsertBatchValue(index,field);
                }).collect(Collectors.joining(Constant.COMMA));
                sql.append(Constant.SMALL_LEFT).append(values).append(Constant.SMALL_RIGHT).append(Constant.COMMA);
            }
            return sql.substring(0,sql.length()-1);
        }catch (Exception e){
            e.printStackTrace();
            throw new MybatisxException("Entity %s Error",object.getClass().getName());
        }
    }

    /**
     * 获取占位符
     * @param field
     * @return
     */
    public String getInsertAndUpdateValue(Field field){

        Column column = field.getAnnotation(Column.class);
        if(column == null || StringUtils.isEmpty(column.jdbcType())){
            return String.format("#{ param.object.%s }",field.getName());
        }else {
            return String.format("#{ param.object.%s , jdbcType = %s }",field.getName(),column.jdbcType());
        }
    }


    /**
     * 获取占位符
     * @param field
     * @return
     */
    public String getInsertBatchValue(int index,Field field){

        Column column = field.getAnnotation(Column.class);
        if(column == null || StringUtils.isEmpty(column.jdbcType())){
            return String.format("#{ param.object[%s].%s }",index,field.getName());
        }else {
            return String.format("#{ param.object[%s].%s , jdbcType = %s }",index,field.getName(),column.jdbcType());
        }
    }


    /**
     * 获取字段对应的数据库字段名称
     * @param field
     * @return
     */
    public String getColumnName(Field field) {
        Column column = field.getAnnotation(Column.class);
        if (column == null || StringUtils.isEmpty(column.value())) {
            return StringUtils.humpTOUnderline(field.getName());
        } else {
            return column.value();
        }
    }


    /**
     * @param properties
     * @param conditions
     * @param param
     * @return java.lang.String
     * @Author 温少
     * @Description 说明： 拼接where条件
     * @Date 2021/4/27 2:17 下午
     **/
    public String getWhere(MybatisxProperties properties, List<Condition> conditions, String param) {
        if (CollectionUtils.isEmpty(conditions)) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < conditions.size(); i++) {
            Condition condition = conditions.get(i);

            if (this.isJoin(condition)) {
                continue;
            }

            String columnName = StringUtils.isEmpty(condition.getProperty()) ? null : StringUtils.humpTOUnderline(condition.getProperty());

            if (StringUtils.isEmpty(columnName) && !this.isAndOr(condition)) {
//            if (StringUtils.isEmpty(columnName)) {
                log.warn("error condition, columnName is null");
                continue;
            }


            /** 处理 and和or */
            if (this.isContainCondition(condition)) {
                sb.append(Constant.SMALL_LEFT).append(this.getWhere(properties, condition.getConditions(), this.getParam(param, i))).append(Constant.SMALL_RIGHT);
                continue;
            }

            /** 处理ON */
            if (Condition.Operator.on.equals(condition.getOperator())) {
                String on = String.format(Constant.ON,
                        this.getTableName(properties, condition.getTable()),
                        columnName,
                        this.getTableName(properties, condition.getTable2()),
                        StringUtils.humpTOUnderline(condition.getProperty2())
                );
                sb.append(on);
                continue;

            }


            if (Condition.Operator.in.equals(condition.getOperator()) || Condition.Operator.nin.equals(condition.getOperator())) {
                Object[] values = condition.getValues();
                StringBuilder stringBuilder = new StringBuilder();
                for (int i1 = 0; i1 < values.length; i1++) {
                    stringBuilder.append(String.format(Constant.VALUE, this.getParam(param, i) + ".values[" + i1 + "]")).append(Constant.COMMA);
                }
                if (Objects.isNull(condition.getTable())) {
                    sb.append(String.format(
                            Constant.WHERE_VALUE_LIST, columnName
                            , condition.getOperator().getValue()
                            , stringBuilder.substring(0, stringBuilder.length() - 1))
                    );
                } else {
                    String tableName = this.getTableName(properties, condition.getTable());
                    sb.append(String.format(
                            Constant.WHERE_TABLE_VALUE_LIST, tableName, columnName
                            , condition.getOperator().getValue()
                            , stringBuilder.substring(0, stringBuilder.length() - 1))
                    );
                }

                continue;
            }


            if (Objects.isNull(condition.getTable())) {
                if (this.isAndOr(condition)) {
                    sb.append(String.format(Constant.WHERE_S, condition.getOperator().getValue()));
                } else if (this.isIsNullOrNotNull(condition)) {
                    sb.append(String.format(Constant.WHERE_VALUE_IS, columnName, condition.getOperator().getValue()));
                } else {
                    sb.append(String.format(Constant.WHERE_VALUE, columnName, condition.getOperator().getValue(), this.getParam(param, i) + ".values[0]"));
                }
            } else {
                String tableName = this.getTableName(properties, condition.getTable());
                if (this.isAndOr(condition)) {
                    sb.append(String.format(Constant.WHERE_S, condition.getOperator().getValue()));
                } else if (this.isIsNullOrNotNull(condition)) {
                    sb.append(String.format(Constant.WHERE_TABLE_VALUE_IS, tableName, columnName, condition.getOperator().getValue()));
                } else {
                    sb.append(String.format(Constant.WHERE_TABLE_VALUE, tableName, columnName, condition.getOperator().getValue(), this.getParam(param, i) + ".values[0]"));
                }
            }
        }

        return sb.toString();
    }

    public String getParam(String param, int index) {
        return param + String.format(Constant.PARAM, index);
    }


    /**
     * @param conditions
     * @param param
     * @return java.lang.String
     * @Author 温少
     * @Description 说明：获取表关联
     * @Date 2021/4/27 2:30 下午
     * * @param properties
     **/
    public void getOn(MybatisxProperties properties, List<Condition> conditions, String param, SQL sql) {
        if (CollectionUtils.isEmpty(conditions)) {
            return;
        }
        for (int i = 0; i < conditions.size(); i++) {
            StringBuilder join = new StringBuilder();
            Condition condition = conditions.get(i);
            if (!this.isJoin(condition)) {
                continue;
            }


            String where = this.getWhere(properties, condition.getConditions(), this.getParam(param, i));

            join.append(String.format(Constant.JOIN,
                    this.getTableName(properties, condition.getTable()),
                    this.getTableName(properties, condition.getTable()),
                    where
            ));
            if (Condition.Operator.inner_join.equals(condition.getOperator())) {
                sql.INNER_JOIN(join.toString());
            } else if (Condition.Operator.left_join.equals(condition.getOperator())) {
                sql.LEFT_OUTER_JOIN(join.toString());
            } else if (Condition.Operator.right_join.equals(condition.getOperator())) {
                sql.RIGHT_OUTER_JOIN(join.toString());
            }
        }
    }


    public Boolean isJoin(Condition condition) {
        return Objects.equals(condition.getOperator(), Condition.Operator.inner_join) || Objects.equals(condition.getOperator(), Condition.Operator.left_join) || Objects.equals(condition.getOperator(), Condition.Operator.right_join);
    }


    public Boolean isAndOr(Condition condition) {
        return Objects.equals(condition.getOperator(), Condition.Operator.and) || Objects.equals(condition.getOperator(), Condition.Operator.or);
    }

    public Boolean isIsNullOrNotNull(Condition condition) {
        return Objects.equals(condition.getOperator(), Condition.Operator.isNull) || Objects.equals(condition.getOperator(), Condition.Operator.isNotNull);
    }

    /**
     * @Author 温少
     * @Description  说明：是否包含条件
     * @Date 2020/12/7 4:31 下午
     * @Param  * @param
     * @return java.lang.Boolean
     **/
    public Boolean isContainCondition(Condition condition){

        if(this.isAndOr(condition)){
            return !CollectionUtils.isEmpty(condition.getConditions());
        }
        return false;

    }

    /**
     * @Author 温少
     * @Description  说明：获取字段是否为空
     * @Date 2020/12/8 6:01 下午
     * @Param  * @param field
     * @param object
     * @return java.lang.Boolean
     **/
    public Boolean getFieldIsNull(Field field , Object object ){
        field.setAccessible(true);
        try {
            Object o = field.get(object);
            if(o == null){
                return true;
            }
            return false;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new MybatisxException("get Field value error");
        }
    }

    /**
     * @Author 温少
     * @Description  说明：设置修改的set
     * @Date 2020/12/8 6:07 下午
     * @Param  * @param isUseNull
     * @param claszz
     * @param object
     * @return java.lang.String
     **/
    public String updateSet(Boolean isUseNull,Class claszz, Object object){

        StringBuilder set = new StringBuilder();
        Field[] allFields = ReflectionUtils.getAllFields(claszz);
        for (Field field : allFields) {
            String columnName = this.getColumnName(field);
            if(this.getFieldIsNull(field,object) && !isUseNull){
                continue;
            }
            set.append(String.format(Constant.SET,columnName,getInsertAndUpdateValue(field))).append(Constant.COMMA);
        }

        return set.length() > 0 ? set.substring(0,set.length()-1) : null;

    }


}
