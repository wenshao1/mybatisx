package com.mybatis.mybatisx.provider;

import com.mybatis.mybatisx.config.MybatisxProperties;
import com.mybatis.mybatisx.constant.Constant;
import com.mybatis.mybatisx.entity.Column;
import com.mybatis.mybatisx.entity.Condition;
import com.mybatis.mybatisx.entity.OrderBy;
import com.mybatis.mybatisx.enums.DataBaseEnum;
import com.mybatis.mybatisx.param.Select;
import com.mybatis.mybatisx.utils.ReflectionUtils;
import com.mybatis.mybatisx.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/5 16:27
 * @Description
 */
@Slf4j
public class SelectProviderBuilder extends BaseProviderBuilder {


    /**
     * @param properties
     * @param clazz
     * @return
     * @Description 获取表名称
     */
    public String getTableName(MybatisxProperties properties, Class clazz, String alias) {
        String tableName = super.getTableName(properties, clazz);
        if (!StringUtils.isEmpty(alias)) {
            return String.format(Constant.TABLE_AS, tableName, alias);
        } else {
            return String.format(Constant.TABLE_AS, tableName, tableName);
        }
    }


    public String selectList(Map<String, Select> params) {
        Select select = params.get("param");
        SQL sql = new SQL();
        sql.SELECT(this.getColumns(select.getMybatisxProperties(), select.getColumns(), select.getResultType()));
        sql.FROM(this.getTableName(select.getMybatisxProperties(), select.getResultType(), null));
        if (this.isExistenceJoin(select.getConditions())) {
            super.getOn(select.getMybatisxProperties(), select.getConditions(), "param", sql);
        }
        if (this.isExistenceWhere(select.getConditions())) {
            sql.WHERE(super.getWhere(select.getMybatisxProperties(), select.getConditions(), "param"));
        }
        if (!CollectionUtils.isEmpty(select.getOrderBys())) {
            sql.ORDER_BY(this.getOrderBy(select.getOrderBys()));
        }
        if (select.getPageable() != null) {
            if (Objects.equals(select.getMybatisxProperties().getDataBaseType(), DataBaseEnum.ORACLE)) {
                return this.buildOraclePage(sql.toString());
            } else {
                sql.LIMIT(this.getLimit());
            }
        }
        if (select.getMybatisxProperties().getIsShowSql()) {
            log.debug(sql.toString());
        }

        return sql.toString();
    }

    public String selectCount(Map<String, Select> params){
        Select select = params.get("param");
        SQL sql = new SQL();
        sql.SELECT(" count(1) ");
        sql.FROM(super.getTableName(select.getMybatisxProperties(),select.getResultType()));
        if(!CollectionUtils.isEmpty(select.getConditions())){
            sql.WHERE(super.getWhere(select.getMybatisxProperties(), select.getConditions(), "param"));
        }
        return sql.toString();
    }

    public String selectListBySql(Map<String, Select> params){
        Select select = params.get("param");
        String sql = select.getSql();
        Assert.notNull(sql,"sql not null !");
        sql = sql.replaceAll("\\?","%s");
        sql = String.format(sql, this.getObjects(select));

        return sql;
    }


    public String getLimit(){
        return String.format(Constant.LIMIT,"param.pageable.row","param.pageable.size");
    }

    public String[] getObjects(Select select){
        if(select.getObjects() == null || select.getObjects().length == 0){
            return new String[0];
        }
        int length = select.getObjects().length;
        String[] strings = new String[length];
        for (int i = 0; i < length; i++) {
            strings[i] = String.format(Constant.OBJECS,i);
        }
        return strings;
    }


    /**
     * @param clazz
     * @return java.lang.String
     * @Author 温少
     * @Description 说明：获取查询的字段
     * @Date 2020/12/9 11:12 上午
     * @Param * @param columns
     **/
    private String getColumns(MybatisxProperties properties, List<Column> columns, Class clazz) {
        StringBuilder sb = new StringBuilder();
        if (CollectionUtils.isEmpty(columns)) {
            Field[] allFields = ReflectionUtils.getAllFields(clazz);
            for (Field field : allFields) {
                String columnName = super.getColumnName(field);
                sb.append(String.format(Constant.COLUMN_ALL, this.getTableName(properties, clazz), columnName, StringUtils.underlineTOHump(columnName))).append(Constant.COMMA);
            }

        } else {
            for (Column column : columns) {
                String c = StringUtils.humpTOUnderline(column.getColumn());
                if(!StringUtils.isEmpty(column.getTable()) && !StringUtils.isEmpty(column.getAlias())){
                    sb.append(String.format(Constant.COLUMN_ALL,column.getTable(),c,StringUtils.underlineTOHump(column.getAlias()))).append(Constant.COMMA);
                }

                if(!StringUtils.isEmpty(column.getTable()) && StringUtils.isEmpty(column.getAlias())){
                    sb.append(String.format(Constant.COLUMN_ALL,column.getTable(),c,StringUtils.underlineTOHump(c))).append(Constant.COMMA);
                }

                if(StringUtils.isEmpty(column.getTable()) && !StringUtils.isEmpty(column.getAlias())){
                    sb.append(String.format(Constant.COLUMN_AS,c,StringUtils.underlineTOHump(column.getAlias()))).append(Constant.COMMA);
                }

                if(StringUtils.isEmpty(column.getTable()) && StringUtils.isEmpty(column.getAlias())){
                    sb.append(String.format(Constant.COLUMN_AS,c,StringUtils.underlineTOHump(c))).append(Constant.COMMA);
                }
            }

        }
        return sb.substring(0,sb.length()-1);
    }

    /**
     * @Author 温少
     * @Description  说明：获取排序值
     * @Date 2020/12/9 11:20 上午
     * @Param  * @param orderBys
     * @return java.lang.String
     **/
    private String getOrderBy(List<OrderBy> orderBys){

        if(CollectionUtils.isEmpty(orderBys)){
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (OrderBy orderBy : orderBys) {
            if(StringUtils.isEmpty(orderBy.getTable())){
                sb.append(String.format(Constant.ORDER,orderBy.getProperty(),orderBy.getDirection().getValue())).append(Constant.COMMA);
            }else {
                sb.append(String.format(Constant.ORDER_T,orderBy.getTable(),orderBy.getProperty(),orderBy.getDirection().getValue())).append(Constant.COMMA);
            }

        }
        return sb.substring(0, sb.length() - 1);
    }


    public String buildOraclePage(String sql) {
        return "SELECT * FROM ( SELECT temp.*, ROWNUM row_id from ( " + sql +
                " ) temp WHERE ROWNUM <= param.pageable.rowEnd ) WHERE row_id > param.pageable.row";
    }

    /**
     * @return java.lang.Boolean
     * @Author 温少
     * @Description 说明：是否包含where条件
     * @Date 2021/4/27 2:35 下午
     * * @param conditions
     **/
    private Boolean isExistenceWhere(List<Condition> conditions) {
        if(CollectionUtils.isEmpty(conditions)){
            return false;
        }
        long count = conditions.stream().filter(condition -> !super.isJoin(condition)).count();
        if (count > 0) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * @return java.lang.Boolean
     * @Author 温少
     * @Description 说明：是否包含表关联条件
     * @Date 2021/4/27 2:35 下午
     * * @param conditions
     **/
    private Boolean isExistenceJoin(List<Condition> conditions) {
        if(CollectionUtils.isEmpty(conditions)){
            return false;
        }
        long count = conditions.stream().filter(condition -> super.isJoin(condition)).count();
        if (count > 0) {
            return true;
        } else {
            return false;
        }
    }

}
