package com.mybatis.mybatisx.constant;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/5 21:59
 * @Description
 */
public class Constant {


    public static final String LEFT = "{";

    public static final String RIGHT = "}";

    public final static String COMMA = ",";

    public final static String WELL = "#";


    public final static String VALUE = " #{ %s } ";

    public final static String VALUE_TYPE = " #{ %s , jdbcType = %s } ";

    public final static String ID = "id";


    public final static String TABLE_AS = " `%s` AS %s ";

    public final static String WHERE_VALUE = " `%s` %s #{ %s } ";

    public final static String WHERE_VALUE_LIST = " `%s` %s ( %s ) ";

    public final static String WHERE_VALUE_IS = " `%s` %s  ";

    public final static String WHERE_TABLE_VALUE = " %s.`%s` %s #{ %s } ";

    public final static String WHERE_TABLE_VALUE_LIST = " %s.`%s` %s ( %s ) ";

    public final static String WHERE_TABLE_VALUE_IS = " `%s.%s` %s  ";

    public final static String WHERE_S = " %s ";

    public static final String SMALL_LEFT = " ( ";

    public static final String SMALL_RIGHT = " ) ";

    public static final String PARAM = ".conditions[%d]";

    public static final String SET = " `%s` = %s ";

    public static final String COLUMN = " `%s` ";

    public static final String COLUMN_AS = " `%s` AS %s ";

    public static final String COLUMN_T = " %s.`%s` ";

    public static final String COLUMN_ALL = " %s.`%s` AS %s ";

    public static final String ORDER = " `%s` %s ";

    public static final String ORDER_T = " %s.`%s` %s ";

    public static final String LIMIT = " #{ %s },#{ %s } ";

    public static final String OBJECS = " #{param.objects[%d]} ";

    public static final String JOIN = " %s AS %s ON ( %s ) ";

    public static final String ON = " %s.%s = %s.%s ";


    /**
     * 默认最大插入条数
     */
    public static final int MAX_PAGE_SIZE = 1000;


}
