package com.mybatis.mybatisx.constant;

/**
 * @ClassName DataSourceConstant
 * @Description
 * @Author 温少
 * @Date 2020/12/11 10:59 上午
 * @Version V1.0
 **/
public class DataSourceConstant {


    public static final String MASTER = "master";

    public static final String DEFAULT_DATA_SOURCE_NAME = "default_data_source";



}
