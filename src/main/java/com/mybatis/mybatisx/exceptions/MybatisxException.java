package com.mybatis.mybatisx.exceptions;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/5 17:05
 * @Description
 */
public class MybatisxException extends RuntimeException{

    public MybatisxException(String message,String... param) {
        super(String.format(message,param));
    }
}
