package com.mybatis.mybatisx.enums;

import lombok.Getter;

/**
 * @ClassName OperationEnum
 * @Description
 * @Author 温少
 * @Date 2020/12/10 11:22 上午
 * @Version V1.0
 **/
public enum OperationEnum {

    UPDATE("update"),
    INSERT("insert"),
    INSERT_BATCH("insert_batch"),
    DELETE("delete"),
    SELECT("select"),

    ;

    @Getter
    private String value;

    OperationEnum(String value){
        this.value = value;
    }
}
