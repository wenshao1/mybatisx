package com.mybatis.mybatisx.enums;

import lombok.Getter;

/**
 * @ClassName DataBaseEnum
 * @Description
 * @Author 温少
 * @Date 2020/12/10 6:31 下午
 * @Version V1.0
 **/
public enum DataBaseEnum {

    MYSQL,
    ORACLE,
    ;
}
