package com.mybatis.mybatisx.utils;

import java.util.List;
import java.util.Objects;

/**
 * @ClassName PageUtil
 * @Description
 * @Author 温少
 * @Date 2021/4/16 5:15 下午
 * @Version V1.0
 **/
public class ListPageUtil {


    /**
     * @Author 温少
     * @Description 说明： 获取总页数
     * @Date 2021/4/16 5:22 下午
     *  * @param total
     * @param pageSize
     * @return int
     **/
    public static int getPage(Integer total,Integer pageSize){
        return (int) Math.ceil((total + pageSize - 1) / pageSize);
    }


    /**
     * 开始分页
     * @param list
     * @param pageNum 页码
     * @param pageSize 每页多少条数据
     * @return
     */
    public static List startPage(List list, Integer pageNum, Integer pageSize) {
        if (list == null) {
            return null;
        }
        if (list.size() == 0) {
            return null;
        }

        Integer count = list.size(); // 记录总数
        Integer pageCount = 0; // 页数
        if (count % pageSize == 0) {
            pageCount = count / pageSize;
        } else {
            pageCount = count / pageSize + 1;
        }

        int fromIndex = 0; // 开始索引
        int toIndex = 0; // 结束索引

        if (!Objects.equals(pageNum,pageCount)) {
            fromIndex = (pageNum - 1) * pageSize;
            toIndex = fromIndex + pageSize;
        } else {
            fromIndex = (pageNum - 1) * pageSize;
            toIndex = count;
        }

        List pageList = list.subList(fromIndex, toIndex);

        return pageList;
    }
}
