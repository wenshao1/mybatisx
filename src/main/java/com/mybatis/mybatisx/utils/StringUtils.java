
package com.mybatis.mybatisx.utils;


import com.google.common.base.CaseFormat;

public class StringUtils extends org.springframework.util.StringUtils {


    //驼峰转下划线
    public static String humpTOUnderline(String sourceStr){
        String result = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, sourceStr);
        return result;
    }

    //下划线转驼峰
    public static String underlineTOHump(String sourceStr){
        String result = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, sourceStr);
        return result;
    }

    /** 首字母大写 */
    public static String firstCapitalize(String s){
        return s.substring(0,1).toUpperCase() + s.substring(1);
    }

    public static void main(String[] args) {
        System.out.println(underlineTOHump("personName"));
    }




}
