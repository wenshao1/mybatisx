package com.mybatis.mybatisx.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/6 0:07
 * @Description
 */
public class ReflectionUtils {


    public static Field[] getAllFields(Class clazz){
        List<Field> list = new ArrayList<>();
        getFields(clazz,list);
        return list.toArray(new Field[list.size()]);

    }




    private static void getFields(Class clazz,List<Field> list){
        Field[] fields = clazz.getDeclaredFields();
        list.addAll(Arrays.asList(fields));
        if(clazz.getSuperclass().equals(Object.class)){
            return;
        }
        getFields(clazz.getSuperclass(),list);
    }


}
