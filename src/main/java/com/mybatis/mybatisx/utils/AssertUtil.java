package com.mybatis.mybatisx.utils;


import org.springframework.util.CollectionUtils;

import java.util.Collection;

/**
 * @ClassName AssertUtil
 * @Description
 * @Author 温少
 * @Date 2021/4/16 2:18 下午
 * @Version V1.0
 **/
public class AssertUtil {


    public static void isEmpty(Collection collection){
        if(CollectionUtils.isEmpty(collection)){
            throw new NullPointerException();
        }
    }
}
