package com.mybatis.mybatisx.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @ClassName BeanUtil
 * @Description
 * @Author 温少
 * @Date 2020/12/9 11:41 上午
 * @Version V1.0
 **/
@Slf4j
public class BeanUtil {

    public static <T> List<T> entityList(Class<T> clazz, List<Map<String, Object>> list) {
        List<T> ts = new ArrayList<>();
        for (Map<String, Object> result : list) {
            ts.add(hashToObject(result, clazz));
        }
        return ts;
    }

    public static <T> T hashToObject(Map<String, Object> map, Class<T> clazz) {
        if (map == null || map.isEmpty()) {
            return null;
        }
        try {
            T obj = clazz.newInstance();
            BeanUtils.populate(obj, map);
            return obj;
        } catch (InvocationTargetException | InstantiationException | IllegalAccessException e) {
            log.error("To object error. class:{}", clazz.getName());
            throw new RuntimeException(e);
        }
    }

}
