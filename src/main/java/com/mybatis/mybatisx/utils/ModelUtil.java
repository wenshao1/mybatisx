package com.mybatis.mybatisx.utils;

import com.mybatis.mybatisx.annotation.Column;
import com.mybatis.mybatisx.constant.Constant;

import java.lang.reflect.Field;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/27 18:59
 * @Description
 */
public class ModelUtil {


    public static String getColumns(Class clazz,String tableAlias){
        StringBuilder sb = new StringBuilder();
        Field[] allFields = ReflectionUtils.getAllFields(clazz);
        for (Field field : allFields) {
            String columnName = getColumnName(field);
            if(StringUtils.isEmpty(tableAlias)){
                sb.append(String.format(Constant.COLUMN_AS,columnName,StringUtils.underlineTOHump(columnName))).append(Constant.COMMA);
            }else {
                sb.append(String.format(Constant.COLUMN_ALL,tableAlias,columnName,StringUtils.underlineTOHump(columnName))).append(Constant.COMMA);
            }
        }
        return sb.substring(0,sb.length()-1);
    }

    public static String getColumnName(Field field){
        Column column = field.getAnnotation(Column.class);
        if(column == null || StringUtils.isEmpty(column.value())){
            return StringUtils.humpTOUnderline(field.getName());
        }else {
            return column.value();
        }
    }

}
