
package com.mybatis.mybatisx.rest;


import com.mybatis.mybatisx.constant.Constant;
import lombok.Data;

@Data
public class Pageable {

    private int page = 1;

    private int size = 20;

    private int row = 0;

    private int endRow = 0;

    public static Pageable start(Integer page,Integer size){
        if(page == null ){
            return null;
        }
        size = size == null ? 20 : size ;
        page = page <= 0 ? 1 : page;
        Pageable pageable = new Pageable();
        pageable.setPage(page);
        pageable.setSize(size);
        pageable.setRow((page-1) * size);
        pageable.setEndRow(pageable.getRow()+size);
        return pageable;
    }

    public String getLimit(){
        return String.format(Constant.LIMIT,row,size);
    }
}
