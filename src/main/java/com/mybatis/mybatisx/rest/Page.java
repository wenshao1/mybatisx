package com.mybatis.mybatisx.rest;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Page<T> implements Serializable {
    private static final long serialVersionUID = -8719253484057263862L;

    /** 总记录数 */
    private long totalCount;

    /** 当前页数 */
    private int page = 1;

    /** 每页条数 */
    private int size = 20;

    private List<T> list = new ArrayList();

    public Page(List<T> list, long totalCount, int pn, int ps) {
        this.list.addAll(list);
        this.totalCount = totalCount;
        this.page = pn;
        this.size = ps;
    }

    public int getTotalPages() {
        return (int) Math.ceil((double) this.getTotalCount() / (double) this.getSize());
    }
}
