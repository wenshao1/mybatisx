package com.mybatis.mybatisx.mapper;

import com.mybatis.mybatisx.entity.PrimaryKeyEntity;
import com.mybatis.mybatisx.param.Delete;
import com.mybatis.mybatisx.param.Insert;
import com.mybatis.mybatisx.param.Select;
import com.mybatis.mybatisx.param.Update;
import com.mybatis.mybatisx.provider.DeleteProviderBuilder;
import com.mybatis.mybatisx.provider.InsertProviderBuilder;
import com.mybatis.mybatisx.provider.SelectProviderBuilder;
import com.mybatis.mybatisx.provider.UpdateProviderBuilder;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface BaseMapper {


    @SelectKey(
            statement = {"select last_insert_id()"},
            keyProperty = "param.object.id",
            before = false,
            resultType = Long.class
    )
    @InsertProvider(type = InsertProviderBuilder.class,method = "insert")
    int insert(@Param("param") Insert i);

    @InsertProvider(type = InsertProviderBuilder.class,method = "insertBatch")
    int insertBatch(@Param("param") Insert i);


    @DeleteProvider(type = DeleteProviderBuilder.class,method = "delete")
    int delete(@Param("param") Delete delete);

    @UpdateProvider(type = UpdateProviderBuilder.class,method = "update")
    int update(@Param("param") Update update);


    @SelectProvider(type = SelectProviderBuilder.class,method = "selectList")
    List<Map<String,Object>> selectList(@Param("param") Select select);


    @SelectProvider(type = SelectProviderBuilder.class,method = "selectCount")
    long selectCount(@Param("param") Select select);

    @SelectProvider(type = SelectProviderBuilder.class,method = "selectListBySql")
    List<Map<String,Object>> selectListBySql(@Param("param") Select select);

/*
    List<Map<String,Object>> selectList(@Param("param") Select select);



    List<Map<String,Object>> selectSum(@Param("param") Select select);*/





}
