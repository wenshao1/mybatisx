package com.mybatis.mybatisx.annotation;

import com.mybatis.mybatisx.constant.DataSourceConstant;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * @Author 温少
 * @Description  说明：数据源注解
 * @Date 2020/12/11 5:34 下午
 * @Param  * @param null
 * @return
 **/
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface DBType {

    /**
     * 使用的数据源
     */
    String name() default DataSourceConstant.MASTER;
}
