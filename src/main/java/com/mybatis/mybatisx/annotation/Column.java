package com.mybatis.mybatisx.annotation;

import java.lang.annotation.*;

/**
 *
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/5 15:49
 * @Description 列注解
 */
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Column {
    String value() default "";

    String jdbcType() default "";

    String comment() default "";
}
