package com.mybatis.mybatisx.annotation;

import java.lang.annotation.*;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/5 15:51
 * @Description
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PrimaryKey {
    String value() default "auto";
}
