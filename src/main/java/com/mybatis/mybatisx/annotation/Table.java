package com.mybatis.mybatisx.annotation;


import java.lang.annotation.*;

/**
 *
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/5 15:49
 * @Description 表注解
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Table {

    String value() default "";

    String comment() default "";

}
