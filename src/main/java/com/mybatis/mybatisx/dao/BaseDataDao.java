package com.mybatis.mybatisx.dao;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mybatis.mybatisx.utils.BeanUtil;
import com.mybatis.mybatisx.utils.StringUtils;
import org.springframework.util.CollectionUtils;

import java.security.Key;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @ClassName BaseDataDao
 * @Description
 * @Author 温少
 * @Date 2020/12/9 11:37 上午
 * @Version V1.0
 **/
public interface BaseDataDao {

    default <T> List<T> resultList(Class<T> clazz ,List<Map<String, Object>> result){

        if(CollectionUtils.isEmpty(result)){
            return Lists.newArrayList();
        }

        if(clazz == Map.class){
            return (List<T>) result;
        }
        return BeanUtil.entityList(clazz, result);
    }

    default List<Map<String, Object>> resultFieldMapping(List<Map<String, Object>> result){
        if(CollectionUtils.isEmpty(result)){
            return result;
        }
        List<Map<String, Object>> data = Lists.newArrayList();
        result.stream().forEach(x->{
            Map<String, Object> map = Maps.newHashMap();
            x.forEach((Key,value)->{
                map.put(StringUtils.underlineTOHump(Key),value);
            });
            data.add(map);
        });
        return data;

    }
}
