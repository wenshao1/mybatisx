package com.mybatis.mybatisx.dao;

import com.mybatis.mybatisx.config.MybatisxProperties;
import com.mybatis.mybatisx.constant.Constant;
import com.mybatis.mybatisx.entity.Column;
import com.mybatis.mybatisx.entity.Condition;
import com.mybatis.mybatisx.entity.OrderBy;
import com.mybatis.mybatisx.entity.PrimaryKeyEntity;
import com.mybatis.mybatisx.enums.OperationEnum;
import com.mybatis.mybatisx.mapper.BaseMapper;
import com.mybatis.mybatisx.param.Delete;
import com.mybatis.mybatisx.param.Insert;
import com.mybatis.mybatisx.param.Select;
import com.mybatis.mybatisx.param.Update;
import com.mybatis.mybatisx.plugin.Interceptor;
import com.mybatis.mybatisx.rest.Page;
import com.mybatis.mybatisx.rest.Pageable;
import com.mybatis.mybatisx.utils.ListPageUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;

/**
 * @author _程序员大魔王-温少
 * @version 1.0
 * @date 2020/12/5 22:20
 * @Description
 */
public class DataDao implements  BaseDataDao {

    @Setter
    @Getter
    public BaseMapper baseMapper;

    @Setter
    public MybatisxProperties mybatisxProperties;

    @Setter
    public List<Interceptor> interceptorChain;

    public <T extends PrimaryKeyEntity> Integer insert(T t){
        Assert.notNull(t,"【insert】 object not null!");
        this.runInterceptor(OperationEnum.INSERT,t);
        Insert insert = Insert.builder()
                .mybatisxProperties(mybatisxProperties)
                .object(t)
                .build();
        return baseMapper.insert(insert);

    }


    public <T extends PrimaryKeyEntity> Integer insertBatch(List<T> list){
        Assert.notNull(list,"【insert】 object not null!");
        if(CollectionUtils.isEmpty(list)){
            throw new IllegalArgumentException("【insertBatch】 List size > 0 !");
        }
        this.runInterceptor(OperationEnum.INSERT_BATCH,list);
        int page = ListPageUtil.getPage(list.size(), Constant.MAX_PAGE_SIZE);
        Insert insert = Insert.builder()
                .mybatisxProperties(mybatisxProperties)
                .build();
        int success = 0;
        for (int i = 0; i < page; i++) {
            List l = ListPageUtil.startPage(list, i + 1, Constant.MAX_PAGE_SIZE);
            insert.setObject(l);
            success+=baseMapper.insertBatch(insert);
        }

        return success;

    }

    public Integer deleteByPrimaryKey(Class<? extends PrimaryKeyEntity> clazz, Long id){
        Assert.notNull(clazz,"【delete】 class not null !");
        Assert.notNull(id,"【delete】 primaryKey not null !");
        this.runInterceptor(OperationEnum.DELETE,null);
        Delete delete = Delete.builder()
                .conditions(Condition.builder().eq("id", id).build())
                .clazz(clazz)
                .isPseudoDel(false)
                .mybatisxProperties(mybatisxProperties).build();
        return baseMapper.delete(delete);
    }

    public Integer deleteByConditions(Class<? extends PrimaryKeyEntity> clazz, List<Condition> conditions){
        Assert.notNull(clazz,"【delete】 class not null !");
        this.runInterceptor(OperationEnum.DELETE,null);
        Delete delete = Delete.builder()
                .conditions(conditions)
                .clazz(clazz)
                .isPseudoDel(false)
                .mybatisxProperties(mybatisxProperties).build();
        return baseMapper.delete(delete);
    }

    public Integer updateByPrimaryKey(PrimaryKeyEntity t){
        Assert.notNull(t,"【update】 object not null !");
        Assert.notNull(t.getId(),"【update】 primaryKey not null !");
        this.runInterceptor(OperationEnum.UPDATE,t);
        Update<PrimaryKeyEntity> update = Update.builder()
                .primaryKeyEntity(t)
                .isUseNull(true)
                .object(t)
                .mybatisxProperties(mybatisxProperties)
                .clazz(t.getClass())
                .conditions(Condition.builder().eq("id", t.getId()).build())
                .build();
        return baseMapper.update(update);
    }

    public Integer updateByPrimaryKeySelective(PrimaryKeyEntity t){
        Assert.notNull(t,"【update】 object not null !");
        Assert.notNull(t.getId(),"【update】 primaryKey not null !");
        this.runInterceptor(OperationEnum.UPDATE,t);
        Update<PrimaryKeyEntity> update = Update.builder()
                .primaryKeyEntity(t)
                .isUseNull(false)
                .object(t)
                .mybatisxProperties(mybatisxProperties)
                .clazz(t.getClass())
                .conditions(Condition.builder().eq("id", t.getId()).build())
                .build();
        return baseMapper.update(update);
    }

    public Integer updateByConditions(PrimaryKeyEntity t, List<Condition> conditions){
        Assert.notNull(t,"【update】 object not null !");
        this.runInterceptor(OperationEnum.UPDATE,t);
        Update<PrimaryKeyEntity> update = Update.builder()
                .primaryKeyEntity(t)
                .isUseNull(true)
                .object(t)
                .mybatisxProperties(mybatisxProperties)
                .clazz(t.getClass())
                .conditions(conditions)
                .build();
        return baseMapper.update(update);
    }

    public Integer updateByConditionsSelective(PrimaryKeyEntity t, List<Condition> conditions){
        Assert.notNull(t,"【update】 object not null !");
        this.runInterceptor(OperationEnum.UPDATE,t);
        Update<PrimaryKeyEntity> update = Update.builder()
                .primaryKeyEntity(t)
                .isUseNull(false)
                .object(t)
                .mybatisxProperties(mybatisxProperties)
                .clazz(t.getClass())
                .conditions(conditions)
                .build();
        return baseMapper.update(update);
    }


    public <E> E selectByPrimaryKey(Class<E> clazz, Long id){
        Assert.notNull(id,"【select】 primaryKey not null !");

        return this.selectOne(clazz,Condition.builder().eq(Constant.ID,id).build());
    }

    public <E> E selectOne(Class<E> clazz, List<Condition> conditions){
        return this.selectOne(clazz,null,conditions);
    }

    public <E> E selectOne(Class<E> clazz, List<Column> columns, List<Condition> conditions){
        List<E> list = this.selectList(clazz, columns, conditions, null,1,1);
        if(!CollectionUtils.isEmpty(list)){
            return list.get(0);
        }
        return null;
    }

    public <E> List<E> selectPageList(Class<E> clazz, List<Column> columns, List<Condition> conditions,Integer page,Integer size){
        return this.selectList(clazz,  columns, conditions,  null, page, size);
    }

    public <E> List<E> selectAll(Class<E> clazz){
        return this.selectPageList(clazz,   null, null);
    }

    public <E> List<E> selectPageList(Class<E> clazz,  Integer page,Integer size){
        return this.selectListPage(clazz,   null, page, size);
    }

    public <E> List<E> selectListPageByCondition(Class<E> clazz, List<Condition> conditions,  Integer page,Integer size){
        return this.selectListAllColumn(clazz, conditions,null,  page, size);
    }

    public <E> List<E> selectList(Class<E> clazz, List<Column> columns ,List<Condition> conditions){
        return this.selectList(clazz,  columns, conditions, null);
    }

    public <E> List<E> selectListPage(Class<E> clazz,   List<OrderBy> orderBys,Integer page,Integer size){
        return this.selectListAllColumn(clazz,   null, orderBys, page, size);
    }

    public <E> List<E> selectListAllColumn(Class<E> clazz,  List<Condition> conditions, List<OrderBy> orderBys,Integer page,Integer size){
        return this.selectList(clazz,  null, conditions, orderBys, page, size);
    }

    public <E> List<E> selectList(Class<E> clazz,  List<OrderBy> orderBys ){
        return this.selectListAllColumn(clazz,   null, orderBys);
    }

    public <E> List<E> selectListByCondition(Class<E> clazz, List<Condition> conditions){
        return this.selectListAllColumn(clazz, conditions,null);
    }

    public <E> List<E> selectListAllColumn(Class<E> clazz,  List<Condition> conditions, List<OrderBy> orderBys){
        return this.selectList(clazz,  null, conditions, orderBys);
    }

    public <E> List<E> selectList(Class<E> clazz, List<Column> columns, List<Condition> conditions, List<OrderBy> orderBys){
        return this.selectList(clazz,  columns, conditions, orderBys, null, null);
    }

    public <E> List<E> selectList(Class<E> clazz, List<Column> columns, List<Condition> conditions, List<OrderBy> orderBys,Integer page,Integer size){
        Assert.notNull(clazz,"【select】 class not null !");
        this.runInterceptor(OperationEnum.SELECT,null);
        Select select = Select.builder()
                .mybatisxProperties(mybatisxProperties)
                .resultType(clazz)
                .columns(columns)
                .conditions(conditions)
                .pageable(Pageable.start(page,size))
                .orderBys(orderBys)
                .build();
        List<Map<String, Object>> data = baseMapper.selectList(select);

        return resultList(clazz,data);

    }


    public <E> Page<E> selectPage(Class<E> clazz,Integer page, Integer size){
        return this.selectPage(clazz,   null, page, size);
    }

    public <E> Page<E> selectPage(Class<E> clazz, List<Condition> conditions, Integer page, Integer size){
        return this.selectPage(clazz,   conditions, null, page, size);
    }

    public <E> Page<E> selectPage(Class<E> clazz, List<Condition> conditions, List<OrderBy> orderBys, Integer page, Integer size){
        return this.selectPage(clazz, null,  conditions, orderBys, page, size);
    }

    public <E> Page<E> selectPage(Class<E> clazz, List<Column> columns, List<Condition> conditions, List<OrderBy> orderBys, Integer page, Integer size){
        Assert.notNull(clazz,"【selectPage】 class not null !");
        this.runInterceptor(OperationEnum.SELECT,null);
        Select select = Select.builder()
                .mybatisxProperties(mybatisxProperties)
                .resultType(clazz)
                .columns(columns)
                .conditions(conditions)
                .orderBys(orderBys)
                .pageable(Pageable.start(page,size))
                .build();
        List<Map<String, Object>> data = baseMapper.selectList(select);
        long total = baseMapper.selectCount(select);
        List<E> list = resultList(clazz, data);
        return new Page(list,total,page,size);
    }

    /**
     * @Author 温少
     * @Description  说明：sql方式查询数据
     * @Date 2020/12/15 3:56 下午
     * @Param  * @param clazz
     * @param sql
     * @param objects
     * @return java.util.List<E>
     **/
    public <E> List<E> selectListBySql(Class<E> clazz,String sql,Object... objects){
        Assert.notNull(sql,"【selectListBySql】 sql not null !");
        this.runInterceptor(OperationEnum.SELECT,null);
        Select select = Select.builder()
                .mybatisxProperties(mybatisxProperties)
                .sql(sql)
                .objects(objects)
                .build();
        List<Map<String, Object>> data = baseMapper.selectListBySql(select);
        data = this.resultFieldMapping(data);
        return resultList(clazz, data);
    }


    /**
     * @Author 温少
     * @Description  说明：统计数据
     * @Date 2020/12/22 11:44 上午
     * @Param  * @param clazz
     * @param conditions
     * @return java.lang.Long
     **/
    public Long selectCount(Class clazz,  List<Condition> conditions){
        Assert.notNull(clazz,"【selectPage】 class not null !");
        this.runInterceptor(OperationEnum.SELECT,null);
        Select select = Select.builder()
                .mybatisxProperties(mybatisxProperties)
                .resultType(clazz)
                .conditions(conditions)
                .build();
        List<Map<String, Object>> data = baseMapper.selectList(select);
        long total = baseMapper.selectCount(select);
        return total;
    }


    /**
     * @Author 温少
     * @Description  说明：执行拦截器
     * @Date 2020/12/10 2:40 下午
     * @Param  * @param operationEnum
     * @param object
     * @return void
     **/
    public void runInterceptor(OperationEnum operationEnum,Object object){
        if(CollectionUtils.isEmpty(interceptorChain)){
            return;
        }
        for (Interceptor interceptor : interceptorChain) {
            interceptor.intercept(this.mybatisxProperties,operationEnum,object);
        }
    }


}
